.data
temp: .word 255                                         # declare data and set to 255
.text
.globl main

main:
                                                        # Begin ALU tests
    addi    $t0,            $zero,  5                   # Init $t0 to 5
    add     $t1,            $zero,  $t0                 # Copy $t0 to $t1
    addu    $t1,            $t1,    $t1                 # Double $t1
    sll     $t0,            $t0,    1                   # Double $t0 by shifting
    sub     $t2,            $t1,    $t0                 # Subtract $t0 from $t1 and put in $t2, should be 0x0
    andi    $t2,            $t2,    0xFFFFFFFF          # And $t2 with all 1s, should still be zero
    ori     $t3,            $t2,    0xFFFFFFFF          # Or $t2 with all 1s and put in $t3, should be all 1s now
    or      $t3,            $t2,    $t3                 # Or $t2 and $t3, should still be all 1s
    sra     $t3,            $t3,    31                  # Shift right preserving sign, should still be all 1s
    xor     $t3,            $t3,    $t2                 # Xor all 1s with all 0s, should still be all 1s
    and     $t4,            $t2,    $t3                 # And all 0s with all 1s and put in $t4, should be all 1s
    xori    $t4,            $t4,    0                   # xor all 1s with all 0s, should be all ones still
    srl     $t5,            $t4,    1                   # $t5 = $t4 >> 1
    sll     $t5,            $t5,    1                   # $t5 = $t5 << 1
    not     $t5,            $t5                         # $t5 should now be 0x00000001
    slt     $t6,            $t3,    $zero               # $t6 = ($t3 < $zero) ? 1 : 0, should be 0x1
    slti    $t7,            $t3,    255                 # $t7 = ($t3  2550) ? 1 : 0, should be 0x1
    addiu   $s0,            $t6,    -1                  # $s0 should be 0x0
    lui     $s1,            0xFFFF                      # $s1 should be 0xFFFF0000
    ori     $s1,            0xFFFF                      # $s1 should now be all 1s
    nor     $s2,            $s1,    $zero               # $s2 = ~($s1 | $zero), should be all zero
    repl.qb $s3,            0xFF                        # $s3 should be all ones

data_instr:
    lw      $s4,            temp                        # $s4 should now be 255
    la      $s5,            temp
    sw      $s4,            4($s5)                      # Memory location one word after temp should now have 255 in it


unconditional:
                                                        # Start control flow tests
    j       jump_and_link

jump_and_link:
    jal     conditionals
    halt    

conditionals:
    beq     $t6,            $t7,    conditional_bne     # if $t7 == $t7 then conditional_bne
    j       end                                         # Shouldn't ever execute

conditional_bne:
    bne     $t7,            $zero,  jmp_reg
    j       end                                         # Shouldn't ever execute

jmp_reg:

# Return to jump_and_link
    jr      $ra

end: