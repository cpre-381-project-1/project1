-------------------------------------------------------------------------
-- Henry Duwe
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- MIPS_Processor.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a skeleton of a MIPS_Processor  
-- implementation.

-- 01/29/2019 by H3::Design created.
-------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

library work;
use work.MIPS_types.all;

entity MIPS_Processor is
  generic (N : integer := DATA_WIDTH);
  port (
    iCLK      : in std_logic;
    iRST      : in std_logic;
    iInstLd   : in std_logic;
    iInstAddr : in std_logic_vector(N - 1 downto 0);
    iInstExt  : in std_logic_vector(N - 1 downto 0);
    oALUOut   : out std_logic_vector(N - 1 downto 0)); -- TODO: Hook this up to the output of the ALU. It is important for synthesis that you have this output that can effectively be impacted by all other components so they are not optimized away.

end MIPS_Processor;
architecture structure of MIPS_Processor is

  -- Required data memory signals
  signal s_DMemWr       : std_logic;                        -- TODO: use this signal as the final active high data memory write enable signal
  signal s_DMemAddr     : std_logic_vector(N - 1 downto 0); -- TODO: use this signal as the final data memory address input
  signal s_DMemData     : std_logic_vector(N - 1 downto 0); -- TODO: use this signal as the final data memory data input
  signal s_DMemOut      : std_logic_vector(N - 1 downto 0); -- TODO: use this signal as the data memory output

  -- Required register file signals 
  signal s_RegWr        : std_logic;                        -- TODO: use this signal as the final active high write enable input to the register file
  signal s_RegWrAddr    : std_logic_vector(4 downto 0);     -- TODO: use this signal as the final destination register address input
  signal s_RegWrData    : std_logic_vector(N - 1 downto 0); -- TODO: use this signal as the final data memory data input

  -- Required instruction memory signals
  signal s_IMemAddr     : std_logic_vector(N - 1 downto 0); -- Do not assign this signal, assign to s_NextInstAddr instead
  signal s_NextInstAddr : std_logic_vector(N - 1 downto 0); -- TODO: use this signal as your intended final instruction memory address input.
  signal s_Inst         : std_logic_vector(N - 1 downto 0); -- TODO: use this signal as the current instruction signal 

  -- Required halt signal -- for simulation
  signal s_Halt         : std_logic;                        -- TODO: this signal indicates to the simulation that intended program execution has completed. (Opcode: 01 0100)

  -- Required overflow signal -- for overflow exception detection
  signal s_Ovfl         : std_logic;                        -- TODO: this signal indicates an overflow exception would have been initiated

  component mem is
    generic (
      ADDR_WIDTH : integer;
      DATA_WIDTH : integer);
    port (
      clk  : in std_logic;
      addr : in std_logic_vector((ADDR_WIDTH - 1) downto 0);
      data : in std_logic_vector((DATA_WIDTH - 1) downto 0);
      we   : in std_logic := '1';
      q    : out std_logic_vector((DATA_WIDTH - 1) downto 0));
  end component;

  -- TODO: You may add any additional signals or components your implementation 
  --       requires below this comment

  component fetch is
    port (
      clk              : in std_logic;                      -- Clock signal
      jmp              : in std_logic;                      -- Whether to jump
      jr               : in std_logic;                      -- Whether to jump with register input
      jmp_reg          : in std_logic_vector(31 downto 0);  -- The jump address from a register if jr
      sign_extend_imm  : in std_logic_vector(31 downto 0);  -- Sign extension of last 16 bits of instruction for j format
      branch           : in std_logic;                      -- Whether we're executing a branch instruction
      zero             : in std_logic;                      -- Whether the ALU output 0
      bne              : in std_logic;                      -- Whether the executed branch instruction is bne
      instr            : in std_logic_vector(31 downto 0);  -- current instruction
      rst              : in std_logic;                      -- Reset PC to a given address
      rst_addr         : in std_logic_vector(31 downto 0);  -- The address to reset PC to
      next_instr_addr  : out std_logic_vector(31 downto 0); -- The address of the next instruction
      pc_plus_4_output : out std_logic_vector(31 downto 0)
    );
  end component;

  component regfile is
    port (
      clk              : in std_logic;
      rst              : in std_logic;
      write_enable     : in std_logic;
      write_addr       : in std_logic_vector(4 downto 0);
      write_data       : in std_logic_vector(31 downto 0);
      read_port_a_addr : in std_logic_vector(4 downto 0);
      read_port_a_data : out std_logic_vector(31 downto 0);
      read_port_b_addr : in std_logic_vector(4 downto 0);
      read_port_b_data : out std_logic_vector(31 downto 0)
    );
  end component;
  component nbit_mux_mto1 is
    generic (
      INPUT_PORTS : positive := 32;
      N           : natural  := 32
    );
    port (
      i_S      : in std_logic_vector(integer(ceil(log2(real(INPUT_PORTS)))) - 1 downto 0);
      i_Input  : in std_logic_vector((N * INPUT_PORTS) - 1 downto 0);
      o_Output : out std_logic_vector(N - 1 downto 0)
    );
  end component;

  component mux2to1_N is
    generic (N : integer := 16); -- Generic of type integer for input/output data width. Default value is 32.
    port (
      i_S  : in std_logic;
      i_D0 : in std_logic_vector(N - 1 downto 0);
      i_D1 : in std_logic_vector(N - 1 downto 0);
      o_O  : out std_logic_vector(N - 1 downto 0));

  end component;

  component ones_comp is
    generic (n : positive);
    port (
      i_X : in std_logic_vector(n - 1 downto 0);
      o_Y : out std_logic_vector(n - 1 downto 0)
    );
  end component;

  component controlModule is
    port (
      opCode     : in std_logic_vector(5 downto 0);
      funct      : in std_logic_vector(5 downto 0);
      ALUSrc     : out std_logic;
      ALUOp      : out std_logic_vector(3 downto 0);
      MemtoReg   : out std_logic;
      MemWrite   : out std_logic;
      MemRead    : out std_logic;
      RegWrite   : out std_logic;
      RegDst     : out std_logic_vector(1 downto 0); --FIXME IN MODULE
      Jump       : out std_logic;
      Branch     : out std_logic;
      Shift      : out std_logic;
      SignExtend : out std_logic;
      OverflowEn : out std_logic;
      Halt       : out std_logic;
      opSlt      : out std_logic;
      opJal      : out std_logic;
      opJr       : out std_logic;
      opBne      : out std_logic;
      opReplQb   : out std_logic);
  end component;

  component ALU is
    port (
      inputA    : in std_logic_vector(31 downto 0);
      inputB    : in std_logic_vector(31 downto 0);
      overflowEn: in std_logic;
      opSelect  : in std_logic_vector(3 downto 0);
      zeroOut   : out std_logic; -- 1 when resultOut = 0
      overflow  : out std_logic;
      resultOut : out std_logic_vector(31 downto 0));
  end component;

  component n_to_m_extender is
    generic (
      N : natural := 16;
      M : natural := 32
    );
    port (
      sign_extend : in std_logic;
      X           : in std_logic_vector(N - 1 downto 0);
      Y           : out std_logic_vector(M - 1 downto 0)
    );
  end component;

  -- Control unit outputs
  signal branch                 : std_logic;
  signal bne                    : std_logic;
  signal jmp                    : std_logic;
  signal memread                : std_logic;
  signal mem2reg                : std_logic;
  signal memwrite               : std_logic;
  signal alu_op                 : std_logic_vector(3 downto 0);
  signal alu_src                : std_logic;
  signal reg_dst                : std_logic_vector(1 downto 0);
  signal shift                  : std_logic;
  signal sign_extend            : std_logic;
  signal overflow_enable        : std_logic;
  signal slt                    : std_logic;
  signal jal                    : std_logic;
  signal jr                     : std_logic;
  signal repl_qb                : std_logic;

  signal fetch_rst_addr         : std_logic_vector(31 downto 0);

  -- Regfile signals
  signal reg_a_output           : std_logic_vector(31 downto 0);
  signal reg_b_output           : std_logic_vector(31 downto 0);
  signal reg_read_a_addr        : std_logic_vector(4 downto 0);
  signal reg_read_b_addr        : std_logic_vector(4 downto 0);
  signal reg_write_addr         : std_logic_vector(4 downto 0);
  signal reg_write_data         : std_logic_vector(31 downto 0);
  signal reg_we                 : std_logic;
  signal reg_rst                : std_logic;

  -- Regfile mux signals
  signal reg_write_addr_mux_1_o : std_logic_vector(4 downto 0);
  -- signal reg_write_addr_mux_2_o : std_logic_vector(4 downto 0);
  signal pc_plus_4              : std_logic_vector(31 downto 0);

  -- ALU signals
  signal alu_zero               : std_logic;
  signal alu_input_a            : std_logic_vector(31 downto 0);
  signal alu_input_b            : std_logic_vector(31 downto 0);
  signal sign_extend_imm        : std_logic_vector (31 downto 0);
  signal imm_16_bit             : std_logic_vector(15 downto 0);
  signal alu_output             : std_logic_vector(31 downto 0);

  -- ALU mux signals
  signal alu_input_b_mux_1      : std_logic_vector(31 downto 0);
  signal alu_shamt_extended     : std_logic_vector(31 downto 0);

  -- Data mem mux signals
  signal dmem_data_mux_1_out    : std_logic_vector(31 downto 0);
  signal datapath_output        : std_logic_vector (31 downto 0);
  signal slt_alu_output         : std_logic_vector(31 downto 0);

begin

  -- TODO: This is required to be your final input to your instruction memory. This provides a feasible method to externally load the memory module which means that the synthesis tool must assume it knows nothing about the values stored in the instruction memory. If this is not included, much, if not all of the design is optimized out because the synthesis tool will believe the memory to be all zeros.
  with iInstLd select
    s_IMemAddr <= s_NextInstAddr when '0',
    iInstAddr when others;
  IMem : mem
  generic map(
    ADDR_WIDTH => ADDR_WIDTH,
    DATA_WIDTH => N)
  port map(
    clk  => iCLK,
    addr => s_IMemAddr(11 downto 2),
    data => iInstExt,
    we   => iInstLd,
    q    => s_Inst);

  DMem : mem
  generic map(
    ADDR_WIDTH => ADDR_WIDTH,
    DATA_WIDTH => N)
  port map(
    clk  => iCLK,
    addr => s_DMemAddr(11 downto 2),
    data => s_DMemData,
    we   => s_DMemWr,
    q    => s_DMemOut);

  -- TODO: Ensure that s_Halt is connected to an output control signal produced from decoding the Halt instruction (Opcode: 01 0100)
  -- TODO: Ensure that s_Ovfl is connected to the overflow output of your ALU

  -- TODO: Implement the rest of your processor below this comment! 

  -- reg_write_addr_concat <= "11111" & s_Inst(15 downto 11) & s_Inst(20 downto 16);
  slt_alu_output     <= alu_output;
  alu_shamt_extended <= x"000000" & "000" & s_Inst(10 downto 6);
  fetch_rst_addr     <= x"00000000";

  reg_read_a_addr    <= s_Inst(25 downto 21);
  reg_read_b_addr    <= s_Inst(20 downto 16);

  -- alu_input_a        <= reg_a_output;

  s_RegWr            <= reg_we;
  s_RegWrAddr        <= reg_write_addr;
  s_RegWrData        <= reg_write_data;
  oALUOut            <= alu_output;

  s_DMemWr           <= memwrite;
  s_DMemAddr         <= alu_output;
  s_DMemData         <= reg_b_output;

  fetch_unit : fetch
  port map(
    clk              => iCLK,
    jmp              => jmp,
    jr               => jr,
    jmp_reg          => reg_a_output,
    sign_extend_imm  => sign_extend_imm,
    branch           => branch,
    zero             => alu_zero,
    bne              => bne,
    instr            => s_Inst,
    rst              => iRST,
    rst_addr         => fetch_rst_addr,
    next_instr_addr  => s_NextInstAddr,
    pc_plus_4_output => pc_plus_4
  );

  registers : regfile
  port map(
    clk              => iClk,
    rst              => iRST,
    write_enable     => reg_we,
    write_addr       => reg_write_addr,
    write_data       => reg_write_data,
    read_port_a_addr => reg_read_a_addr,
    read_port_a_data => reg_a_output,
    read_port_b_addr => reg_read_b_addr,
    read_port_b_data => reg_b_output
  );

  -- reg_write_addr_mux : nbit_mux_mto1
  -- generic map(
  --   3,
  --   5
  --   ) port map (
  --   i_S      => reg_dst,
  --   i_Input  => reg_write_addr_concat,
  --   o_Output => reg_write_addr
  -- );

  reg_write_addr_mux_1 : mux2to1_N
  generic map(5)
  port map(
    reg_dst(0),
    s_Inst(20 downto 16),
    s_Inst(15 downto 11),
    reg_write_addr_mux_1_o
  );

  reg_write_addr_mux_2 : mux2to1_N
  generic map(5)
  port map(
    reg_dst(1),
    reg_write_addr_mux_1_o,
    "11111",
    reg_write_addr
  );
  dmem_output_mux : mux2to1_N
  generic map(32)
  port map(
    i_S  => mem2reg,
    i_D0 => alu_output,
    i_D1 => s_DMemOut,
    o_O  => dmem_data_mux_1_out
  );

  datapath_output_mux : mux2to1_N
  generic map(32)
  port map(
    i_S  => slt,
    i_D0 => dmem_data_mux_1_out,
    i_D1 => slt_alu_output,
    o_O  => datapath_output
  );

  imm_select_mux: mux2to1_N
  generic map(16)
  port map (
    i_S => repl_qb,
    i_D0 => s_Inst(15 downto 0),
    i_D1 => "00000000" & s_Inst(23 downto 16),
    o_O => imm_16_bit
  );

  alu_sign_extender : n_to_m_extender
  generic map(16, 32)
  port map(
    sign_extend => sign_extend,
    X           => imm_16_bit,
    Y           => sign_extend_imm
  );

  alu_src_mux : mux2to1_N
  generic map(32)
  port map(
    i_S  => alu_src,
    i_D0 => reg_b_output,
    i_D1 => sign_extend_imm,
    o_O  => alu_input_b
  );

  alu_shift_select_mux : mux2to1_N
  generic map(32)
  port map(
    i_S  => shift,
    i_D0 => reg_a_output,
    i_D1 => alu_shamt_extended,
    o_O  => alu_input_a
  );

  reg_write_data_mux : mux2to1_N
  generic map(32)
  port map(
    i_S  => jal,
    i_D0 => datapath_output,
    i_D1 => pc_plus_4,
    o_O  => reg_write_data
  );

  control_unit : controlModule
  port map(
    opCode     => s_Inst(31 downto 26),
    funct      => s_Inst(5 downto 0),
    ALUSrc     => alu_src,
    ALUOp      => alu_op,
    MemtoReg   => mem2reg,
    MemWrite   => memwrite,
    MemRead    => memread,
    RegWrite   => reg_we,
    RegDst     => reg_dst,
    Jump       => jmp,
    Branch     => branch,
    Shift      => shift,
    SignExtend => sign_extend,
    OverflowEn => overflow_enable,
    Halt       => s_Halt,
    opSlt      => slt,
    opJal      => jal,
    opJr       => jr,
    opBne      => bne,
    opReplQb   => repl_qb
  );

  arithmetic_logic_unit : ALU
  port map(
    alu_input_a,
    alu_input_b,
    overflow_enable,
    alu_op,
    alu_zero,
    s_Ovfl,
    alu_output
  );

end structure;
