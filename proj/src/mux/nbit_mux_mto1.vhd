-------------------------------------------------------------------------
-- Branden Butler
-- CPRE 381 Lab 1
-- Iowa State University
------------------------------------------------------------------------

-- mux2to1.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file implements a 2-to-1 multiplexer
-- using the dataflow VHDL design pattern. i_Input is represented
-- as a concatenated vector since our version of vhdl doesn't like arrays of arbitrary
-- length vectors

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
	

entity nbit_mux_mto1 is
	generic (
		INPUT_PORTS: positive := 32;
		N: natural := 32
	);
	port (
		i_S : in std_logic_vector(integer(ceil(log2(real(INPUT_PORTS)))) - 1 downto 0);
		i_Input: in std_logic_vector((N * INPUT_PORTS) - 1 downto 0);
		o_Output: out std_logic_vector(N - 1 downto 0)
	);
end nbit_mux_mto1;

architecture behavioral of nbit_mux_mto1 is
	type generic_std_logic_vector_array is array(natural range <>) of std_logic_vector(N - 1 downto 0);
	signal input_array : generic_std_logic_vector_array(INPUT_PORTS - 1 downto 0);
	
begin

	-- Choose which element of the internal array to output
	o_Output <= input_array(to_integer(unsigned(i_S)));

	gen : for i in 0 to INPUT_PORTS - 1 generate
    -- Connect input vector to the internal array
	input_array(i) <= i_Input(((i + 1) * N) - 1 downto (i * N));
  end generate;
  
  


end behavioral;
