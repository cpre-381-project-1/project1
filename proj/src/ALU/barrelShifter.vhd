-------------------------------------------------------------------------
-- Tyler Duquette
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- barrelShifter.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the barrel shifter
--              module of the MIPS processor
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity barrelShifter is
port(shiftIn	: in std_logic_vector(31 downto 0);
     shiftMd    : in std_logic; --0 for Logical/1 for Arithmetic
     shiftDr    : in std_logic; --0 for Right/1 for Left
     shiftNm    : in std_logic_vector(4 downto 0);
     shiftOt    : out std_logic_vector(31 downto 0));
end barrelShifter;


architecture structural of barrelShifter is

--------------------------
--Component Declarations--
--------------------------
component mux2to1_dataflow is
port(i_S     : in std_logic;
     i_D0    : in std_logic;
     i_D1    : in std_logic;
     o_O     : out std_logic);
end component;

component mux2to1_n is
generic(N :natural := 32);
port(i_S     : in std_logic;
     i_D0    : in std_logic_vector(31 downto 0);
     i_D1    : in std_logic_vector(31 downto 0);
     o_O     : out std_logic_vector(31 downto 0));
end component;

-----------
--Signals--
-----------

signal s_SignBit   : std_logic; --Preserve for arithmetic shifting
signal s_SBEr      : std_logic;
signal s_Arth      : std_logic_vector(31 downto 0);

signal s_LeftShft  : std_logic_vector(31 downto 0); --Reversed input for left shifting
signal s_FinR      : std_logic_vector(31 downto 0);


signal s_Init      : std_logic_vector(31 downto 0); 
signal s_P1        : std_logic_vector(31 downto 0);--The vector to be sent through the first shift phase
signal s_P2        : std_logic_vector(31 downto 0);
signal s_P3        : std_logic_vector(31 downto 0);
signal s_P4        : std_logic_vector(31 downto 0);
signal s_P5        : std_logic_vector(31 downto 0);
signal s_Fin       : std_logic_vector(31 downto 0);
signal s_ShiftFill : std_logic;

begin

-----------
--Mapping--
-----------

--Signal Assignment--

s_SignBit <= shiftIn(31); --Store the sign bit for arithmetic shifting
s_ShiftFill <= shiftMd and not shiftDr and s_SignBit; -- Determine what bit we want to shift in, only 1 if sra and sign is 1


s_LeftShft(0) <= shiftIn(31); --Flip the order of bits for performing left shifting
s_LeftShft(1) <= shiftIn(30);
s_LeftShft(2) <= shiftIn(29);
s_LeftShft(3) <= shiftIn(28);
s_LeftShft(4) <= shiftIn(27);
s_LeftShft(5) <= shiftIn(26);
s_LeftShft(6) <= shiftIn(25);
s_LeftShft(7) <= shiftIn(24);
s_LeftShft(8) <= shiftIn(23);
s_LeftShft(9) <= shiftIn(22);
s_LeftShft(10) <= shiftIn(21);
s_LeftShft(11) <= shiftIn(20);
s_LeftShft(12) <= shiftIn(19);
s_LeftShft(13) <= shiftIn(18);
s_LeftShft(14) <= shiftIn(17);
s_LeftShft(15) <= shiftIn(16);
s_LeftShft(16) <= shiftIn(15);
s_LeftShft(17) <= shiftIn(14);
s_LeftShft(18) <= shiftIn(13);
s_LeftShft(19) <= shiftIn(12);
s_LeftShft(20) <= shiftIn(11);
s_LeftShft(21) <= shiftIn(10);
s_LeftShft(22) <= shiftIn(9);
s_LeftShft(23) <= shiftIn(8);
s_LeftShft(24) <= shiftIn(7);
s_LeftShft(25) <= shiftIn(6);
s_LeftShft(26) <= shiftIn(5);
s_LeftShft(27) <= shiftIn(4);
s_LeftShft(28) <= shiftIn(3);
s_LeftShft(29) <= shiftIn(2);
s_LeftShft(30) <= shiftIn(1);
s_LeftShft(31) <= shiftIn(0);



--Left/Right Selection--
DirectionPicker: mux2to1_n 
generic map(32)
port map( --Picks the proper input depending on whether left or right shift is occurring 
                 i_S   => shiftDr,
                 i_D0  => shiftIn,
                 i_D1  => s_LeftShft,
                 o_O   => s_Init);

s_P1  <= s_Init;


--First Shift (1)--

ShiftPhase1: for i in 0 to 30 generate
  MuxShf: mux2to1_dataflow
  port map(i_D0   => s_P1(i),
           i_D1   => s_P1(i+1),
           i_S    => shiftNm(0),
           o_O    => s_P2(i));
end generate ShiftPhase1;

ZeroPhase1: mux2to1_dataflow
  port map(i_D0   => s_P1(31),
           i_D1   => s_ShiftFill,
           i_S    => shiftNm(0),
           o_O    => s_P2(31));



--Second Shift (2)--

ShiftPhase2: for i in 0 to 29 generate
  MuxShf: mux2to1_dataflow
  port map(i_D0   => s_P2(i),
           i_D1   => s_P2(i+2),
           i_S    => shiftNm(1),
           o_O    => s_P3(i));
end generate ShiftPhase2;

ZeroPhase2: for i in 30 to 31 generate
  MuxZr: mux2to1_dataflow
  port map(i_D0   => s_P2(i),
           i_D1   => s_ShiftFill,
           i_S    => shiftNm(1),
           o_O    => s_P3(i));
end generate ZeroPhase2;

--Third Shift (4)--

ShiftPhase3: for i in 0 to 27 generate
  MuxShf: mux2to1_dataflow
  port map(i_D0   => s_P3(i),
           i_D1   => s_P3(i+4),
           i_S    => shiftNm(2),
           o_O    => s_P4(i));
end generate ShiftPhase3;

ZeroPhase3: for i in 28 to 31 generate
  MuxZr: mux2to1_dataflow
  port map(i_D0   => s_P3(i),
           i_D1   => s_ShiftFill,
           i_S    => shiftNm(2),
           o_O    => s_P4(i));
end generate ZeroPhase3;

--Fourth Shift (8)--

ShiftPhase4: for i in 0 to 23 generate
  MuxShf: mux2to1_dataflow
  port map(i_D0   => s_P4(i),
           i_D1   => s_P4(i+8),
           i_S    => shiftNm(3),
           o_O    => s_P5(i));
end generate ShiftPhase4;

ZeroPhase4: for i in 24 to 31 generate
  MuxZr: mux2to1_dataflow
  port map(i_D0   => s_P4(i),
           i_D1   => s_ShiftFill,
           i_S    => shiftNm(3),
           o_O    => s_P5(i));
end generate ZeroPhase4;

--Fifth Shift (16)--

ShiftPhase5: for i in 0 to 15 generate
  MuxShf: mux2to1_dataflow
  port map(i_D0   => s_P5(i),
           i_D1   => s_P5(i+16),
           i_S    => shiftNm(4),
           o_O    => s_Fin(i));
end generate ShiftPhase5;

ZeroPhase5: for i in 16 to 31 generate
  MuxZr: mux2to1_dataflow
  port map(i_D0   => s_P5(i),
           i_D1   => s_ShiftFill,
           i_S    => shiftNm(4),
           o_O    => s_Fin(i));
end generate ZeroPhase5;

--Reverse Bits Back (if left shift)--
s_FinR(0) <= s_Fin(31);
s_FinR(1) <= s_Fin(30);
s_FinR(2) <= s_Fin(29);
s_FinR(3) <= s_Fin(28);
s_FinR(4) <= s_Fin(27);
s_FinR(5) <= s_Fin(26);
s_FinR(6) <= s_Fin(25);
s_FinR(7) <= s_Fin(24);
s_FinR(8) <= s_Fin(23);
s_FinR(9) <= s_Fin(22);
s_FinR(10) <= s_Fin(21);
s_FinR(11) <= s_Fin(20);
s_FinR(12) <= s_Fin(19);
s_FinR(13) <= s_Fin(18);
s_FinR(14) <= s_Fin(17);
s_FinR(15) <= s_Fin(16);
s_FinR(16) <= s_Fin(15);
s_FinR(17) <= s_Fin(14);
s_FinR(18) <= s_Fin(13);
s_FinR(19) <= s_Fin(12);
s_FinR(20) <= s_Fin(11);
s_FinR(21) <= s_Fin(10);
s_FinR(22) <= s_Fin(9);
s_FinR(23) <= s_Fin(8);
s_FinR(24) <= s_Fin(7);
s_FinR(25) <= s_Fin(6);
s_FinR(26) <= s_Fin(5);
s_FinR(27) <= s_Fin(4);
s_FinR(28) <= s_Fin(3);
s_FinR(29) <= s_Fin(2);
s_FinR(30) <= s_Fin(1);
s_FinR(31) <= s_Fin(0);


DirectionPicker2: mux2to1_n
generic map(32)
port map(i_S      => shiftDr,
         i_D0     => s_Fin,
         i_D1     => s_FinR,
         o_O      => s_Arth);

--Restore Sign Bit (if arithmetic shift)--

ArthMux: mux2to1_dataflow
port map(i_D0     => s_Arth(31),
         i_D1     => s_SignBit,
         i_S      => shiftMd,
         o_O      => shiftOt(31));

shiftOt(30 downto 0) <= s_Arth(30 downto 0);



end structural;
