--------------------------------------------------------------------------------------
-- Tyler Duquette
-- Department of Electrical and Computer Engineering
-- Iowa State University
--------------------------------------------------------------------------------------


-- ALU.vhd
--------------------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the ALU of the MIPS processor
--------------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;


entity ALU is
port(inputA       : in std_logic_vector(31 downto 0);
     inputB       : in std_logic_vector(31 downto 0);
     overflowEn   : in std_logic;
     opSelect     : in std_logic_vector(3 downto 0);
     zeroOut      : out std_logic; -- 1 when resultOut = 0
     overflow     : out std_logic;
     resultOut    : out std_logic_vector(31 downto 0));
end ALU;

architecture mixed of ALU is

--------------
--Components--
--------------

--AddSub
component nbit_adder_subtractor is
generic (N : integer := 16);
port(i_A      : in std_logic_vector(31 downto 0);
     i_B      : in std_logic_vector(31 downto 0);
     i_AddSub : in std_logic;
     o_Sum    : out std_logic_vector(31 downto 0);
     o_Cm     : out std_logic;
     o_C      : out std_logic); --Change to add previous carry as output in order to XOR for overflow
end component;

--barrelShift
component barrelShifter is
port(shiftIn	: in std_logic_vector(31 downto 0);
     shiftMd    : in std_logic; --0 for Logical/1 for Arithmetic
     shiftDr    : in std_logic; --0 for Right/1 for Left
     shiftNm    : in std_logic_vector(4 downto 0);
     shiftOt    : out std_logic_vector(31 downto 0));
end component;



--OR
component org2 is
port(i_A	: in std_logic;
     i_B	: in std_logic;
     o_F	: out std_logic);
end component;

--AND
component andg2 is
port(i_A	: in std_logic;
     i_B	: in std_logic;
     o_F	: out std_logic);
end component;

--XOR
component xorg2 is
port(i_A          : in std_logic;
     i_B          : in std_logic;
     o_F          : out std_logic);
end component;

--One's Complementor/NOT
component ones_comp is
     generic(n: positive);
     port(
         i_X: in std_logic_vector(n-1 downto 0);
         o_Y: out std_logic_vector(n-1 downto 0)
     );
end component;





-----------
--Signals--
-----------

signal s_and  : std_logic_vector(31 downto 0);
signal s_or   : std_logic_vector(31 downto 0);
signal s_add  : std_logic_vector(31 downto 0);
signal s_sub  : std_logic_vector(31 downto 0);
signal s_xor  : std_logic_vector(31 downto 0);
signal s_nor  : std_logic_vector(31 downto 0);
signal s_hold : std_logic_vector(31 downto 0);
signal s_slt  : std_logic_vector(31 downto 0);
signal s_sll  : std_logic_vector(31 downto 0);
signal s_srl  : std_logic_vector(31 downto 0);
signal s_sra  : std_logic_vector(31 downto 0);
signal s_rep  : std_logic_vector(31 downto 0);
signal s_not  : std_logic_vector(31 downto 0);
signal s_lui  : std_logic_vector(31 downto 0);
signal s_zero : std_logic_vector(31 downto 0);
signal s_cm   : std_logic;
signal s_co   : std_logic;
signal s_of_detect: std_logic;
-----------
--Mapping--
-----------
begin

--AND--
ALU_AND: for i in 0 to 31 generate
 ANDGS: andg2
 port map(i_A => inputA(i),
          i_B => inputB(i),
          o_F => s_and(i));
end generate ALU_AND;

--OR--
ALU_OR: for i in 0 to 31 generate
 ORGS: org2
 port map(i_A => inputA(i),
          i_B => inputB(i),
          o_F => s_or(i));
end generate ALU_OR;

--ADD--
ALU_ADD: nbit_adder_subtractor
generic map(32)
port map(i_A => inputA,
         i_B => inputB,
         i_AddSub => '0',
         o_Sum => s_add,
         o_Cm  => s_cm,
         o_C   => s_co);

--SUB--
ALU_SUB: nbit_adder_subtractor
generic map(32)
port map(i_A => inputA,
         i_B => inputB,
         i_AddSub => '1',
         o_Sum => s_sub,
         o_C   => open);

--XOR--
ALU_XOR: for i in 0 to 31 generate
 XORGS: xorg2
 port map(i_A => inputA(i),
          i_B => inputB(i),
          o_F => s_xor(i));
end generate ALU_XOR;

--NOR--
ALU_NOR: ones_comp
generic map (32)
port map(i_X => s_or,
         o_Y => s_nor);


--SLT--
ALU_SLT: nbit_adder_subtractor
generic map(32)
port map(i_A => inputA,
         i_B => inputB,
         i_AddSub => '1',
         o_Sum => s_hold,
         o_C => open);

with inputA(31) & inputB(31) & s_hold(31) select
     s_slt <= x"00000000" when "000" | "110" | "010" | "011", 
              x"00000001" when others; 

--SLL--
ALU_SLL: barrelShifter
port map(shiftIn => inputB,
         shiftMd => '0',
         shiftDr => '1',
         shiftNm => inputA(4 downto 0),
         shiftOt => s_sll);
            

--SRL--
ALU_SRL: barrelShifter
port map(shiftIn => inputB,
         shiftMd => '0',
         shiftDr => '0',
         shiftNm => inputA(4 downto 0),
         shiftOt => s_srl);

--SRA--
ALU_SRA: barrelShifter
port map(shiftIn => inputB,
         shiftMd => '1',
         shiftDr => '0',
         shiftNm => inputA(4 downto 0),
         shiftOt => s_sra);

--REPL--
s_rep <= inputB(7 downto 0)&inputB(7 downto 0)&inputB(7 downto 0)&inputB(7 downto 0);

--NOT--
ALU_NOT: ones_comp
generic map(32)
port map(i_X => inputA,
         o_Y => s_not);


--LUI--
s_lui <= inputB(15 downto 0)&"0000000000000000";


--Output Selection--
with opSelect select
     s_zero    <= s_and when "0000",
                  s_or when "0001",
                  s_add when "0010",
                  s_sub when "0011",
                  s_xor when "0100",
                  s_nor when "0101",
                  s_slt when "0110",
                  s_sll when "0111",
                  s_srl when "1000",
                  s_sra when "1001",
                  s_rep when "1010",
                  s_not when "1011",
                  s_lui when "1100",
                  s_and when others;

resultOut <= s_zero;

with s_zero select
     zeroOut <= '1' when x"00000000",
                '0' when others;

--OVERFLOW DETECTION--
ALU_OF: xorg2
port map(i_A => s_co,
         i_B => s_cm,
         o_F => s_of_detect);

overflow <= s_of_detect and overflowEn;




end mixed;