-------------------------------------------------------------------------
-- Branden Butler
-- CPRE 381 Lab 1
-- Iowa State University
------------------------------------------------------------------------

-- n_to_m_extender.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file implements a bit extender from n bits to m bits.
-- The extender can be configured to either zero-extend or sign-extend
-- the input.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity n_to_m_extender is
    generic (
        N : natural := 16;
        M : natural := 32
    );
    port (
        sign_extend : in std_logic;
        X           : in std_logic_vector(N - 1 downto 0);
        Y           : out std_logic_vector(M - 1 downto 0)
    );
end n_to_m_extender;

architecture dataflow of n_to_m_extender is

begin

    with sign_extend select Y <=
        std_logic_vector(resize(unsigned(X), Y'length)) when '0',
        std_logic_vector(resize(signed(X), Y'length)) when '1',
        (Y'length - 1 downto 0 => '0') when others;

end dataflow ; -- dataflow
