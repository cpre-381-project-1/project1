-------------------------------------------------------------------------
-- Branden Butler
-- CprE 381
-- Iowa State University
-------------------------------------------------------------------------


-- ones_comp.vhdl
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a structural
-- one's complementor unit.
--
-- 09/1/22 by BB: Design created
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity ones_comp is
    generic(n: positive);
    port(
        i_X: in std_logic_vector(n-1 downto 0);
        o_Y: out std_logic_vector(n-1 downto 0)
    );
end ones_comp;

architecture structural of ones_comp is
    component invg is

		port(
			i_A          : in std_logic;
			o_F          : out std_logic
		);

	end component;
    begin
        G_NBit_Inv: for i in 0 to n-1 generate
            InvI: invg port map (
                i_A => i_X(i),
                o_F => o_Y(i)
            );
        end generate G_NBit_Inv;

end structural;