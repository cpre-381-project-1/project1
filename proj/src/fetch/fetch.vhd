-------------------------------------------------------------------------
-- Branden Butler
-- CprE 381
-- Project 1
-- Iowa State University
-------------------------------------------------------------------------
-- fetch.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a MIPS processor
-- instruction fetch unit

library IEEE;
use IEEE.std_logic_1164.all;

use ieee.numeric_std.all;

entity fetch is
    port (
        clk             : in std_logic;                      -- Clock signal
        jmp             : in std_logic;                      -- Whether to jump
        jr              : in std_logic;                      -- Whether to jump with register input
        jmp_reg         : in std_logic_vector(31 downto 0);  -- The jump address from a register if jr
        sign_extend_imm : in std_logic_vector(31 downto 0);  -- Sign extension of last 16 bits of instruction for j format
        branch          : in std_logic;                      -- Whether we're executing a branch instruction
        zero            : in std_logic;                      -- Whether the ALU output 0
        bne             : in std_logic;                      -- Whether the executed branch instruction is bne
        instr           : in std_logic_vector(31 downto 0);  -- current instruction
        rst             : in std_logic;                      -- Reset PC to a given address
        rst_addr        : in std_logic_vector(31 downto 0);  -- The address to reset PC to
        next_instr_addr : out std_logic_vector(31 downto 0); -- The address of the next instruction
        pc_plus_4_output: out std_logic_vector(31 downto 0)
    );
end fetch;

architecture structural of fetch is
    component nbit_register is
        generic (N : integer := 16);
        port (
            i_CLK : in std_logic;
            i_RST : in std_logic;
            i_WE  : in std_logic; -- Write enable
            i_D   : in std_logic_vector(N - 1 downto 0);
            o_Q   : out std_logic_vector(N - 1 downto 0)
        );
    end component;

    component nbit_adder_subtractor is
        generic (N : integer := 16);
        port (
            i_A      : in std_logic_vector(N - 1 downto 0);
            i_B      : in std_logic_vector(N - 1 downto 0);
            i_AddSub : in std_logic;
            o_Sum    : out std_logic_vector(N - 1 downto 0);
            o_C      : out std_logic
        );
    end component;

    component mux2to1_N is
        generic(N : integer := 16); -- Generic of type integer for input/output data width. Default value is 32.
        port(i_S          : in std_logic;
             i_D0         : in std_logic_vector(N-1 downto 0);
             i_D1         : in std_logic_vector(N-1 downto 0);
             o_O          : out std_logic_vector(N-1 downto 0));
      
      end component;

    component mux2to1_dataflow is
        port (
            i_S  : in std_logic;
            i_D0 : in std_logic;
            i_D1 : in std_logic;
            o_O  : out std_logic
        );
    end component;

    signal pc_q                : std_logic_vector(31 downto 0);
    signal pc_final            : std_logic_vector(31 downto 0);
    signal pc_plus_4           : std_logic_vector(31 downto 0);
    signal jmp_addr            : std_logic_vector(31 downto 0);
    signal new_pc              : std_logic_vector(31 downto 0);
    signal pc_d                : std_logic_vector(31 downto 0); -- D input to the PC register, after the mux
    signal shifted_imm         : std_logic_vector(31 downto 0); -- Sign extended immediate shifted left 2
    signal branch_target_addr  : std_logic_vector(31 downto 0); -- Calculated branch target from adder
    signal non_jmp_addr        : std_logic_vector(31 downto 0); -- Next PC disregarding jumps, i.e. after first mux
    signal non_reg_addr        : std_logic_vector(31 downto 0); -- Next PC disregarding jr, i.e. after second mux

    signal do_branch           : std_logic;                     -- Whether the branch should happen
    signal branch_and_zero     : std_logic;                     -- Whether both branch and zero
    signal not_branch_and_zero : std_logic;                     -- The inverse of branch_and_zero

    signal add_4_carry         : std_logic;                     -- Unused
    signal branch_adder_carry  : std_logic;                     -- Unused
begin

    next_instr_addr     <= pc_q;
    jmp_addr            <= pc_plus_4(31 downto 28) & instr(25 downto 0) & "00";
    branch_and_zero     <= branch and zero;
    not_branch_and_zero <= not(branch_and_zero);
    shifted_imm         <= sign_extend_imm(29 downto 0) & "00";
    pc_plus_4_output    <= pc_plus_4;

    pc_rst_mux: mux2to1_N
    generic map (32)
    port map (
        rst,
        new_pc,
        rst_addr,
        pc_d
    );

    pc_final <= x"00400000" when rst = '1' else pc_d;

    PC : nbit_register
    generic map(32)
    port map(
        clk,
        '0',
        '1',
        pc_final,
        pc_q
    );

    add_4 : nbit_adder_subtractor
    generic map(32)
    port map(
        pc_q,
        x"00000004",
        '0',
        pc_plus_4,
        add_4_carry
    );

    branch_adder : nbit_adder_subtractor
    generic map(32)
    port map(
        pc_plus_4,
        shifted_imm,
        '0',
        branch_target_addr,
        branch_adder_carry
    );

    bne_select : mux2to1_dataflow
    port map(
        bne,
        branch_and_zero,
        not_branch_and_zero,
        do_branch
    );

    branch_select : mux2to1_N
    generic map(32)
    port map(
        do_branch,
        pc_plus_4,
        branch_target_addr,
        non_jmp_addr
    );

    jmp_or_branch_select : mux2to1_N
    generic map(32)
    port map(
        jmp,
        non_jmp_addr,
        jmp_addr,
        non_reg_addr
    );

    reg_or_other_select : mux2to1_N
    generic map(32)
    port map(
        jr,
        non_reg_addr,
        jmp_reg,
        new_pc
    );
end structural;
