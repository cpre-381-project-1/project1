-------------------------------------------------------------------------
-- Branden Butler
-- CprE 381
-- Iowa State University
-------------------------------------------------------------------------
-- nbit_decoder.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of an n bit
-- decoder using the behavioral design pattern.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity nbit_decoder is
    generic (N : natural := 5);
    port (
        X : in std_logic_vector(N - 1 downto 0);
        Y : out std_logic_vector((2 ** N) - 1 downto 0)
    );
end nbit_decoder;

architecture behavioral of nbit_decoder is

begin
    process (X) -- Whenever X changes, update Y
    begin
        Y                          <= (others => '0'); -- Sets all bits to 0
        Y(to_integer(unsigned(x))) <= '1';             -- Then set the requested bit to 1
    end process;

end behavioral;
