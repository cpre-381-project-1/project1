-------------------------------------------------------------------------
-- Branden Butler
-- CprE 381
-- Iowa State University
-------------------------------------------------------------------------
-- regfile.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a 32-bit register
-- file containing 32 registers, with one write port and two read ports. Register
-- zero always contains the value 0x00000000.

library IEEE;
use IEEE.std_logic_1164.all;

library work;
use work.MIPS_types.all;
entity regfile is
    port (
        clk              : in std_logic;
        rst              : in std_logic;
        write_enable     : in std_logic;
        write_addr       : in std_logic_vector(4 downto 0);
        write_data       : in std_logic_vector(31 downto 0);
        read_port_a_addr : in std_logic_vector(4 downto 0);
        read_port_a_data : out std_logic_vector(31 downto 0);
        read_port_b_addr : in std_logic_vector(4 downto 0);
        read_port_b_data : out std_logic_vector(31 downto 0)
    );
end regfile;

architecture structural of regfile is

    component decoder_5to32 is
        port (
            X : in std_logic_vector(4 downto 0);
            Y : out std_logic_vector(31 downto 0)
        );
    end component;

    component nbit_register is
        generic (N : integer := 16);
        port (
            i_CLK : in std_logic;
            i_RST : in std_logic;
            i_WE  : in std_logic; -- Write enable
            i_D   : in std_logic_vector(N - 1 downto 0);
            o_Q   : out std_logic_vector(N - 1 downto 0)
        );
    end component;

    component mux32to1 is
        port (
            i_S   : in std_logic_vector(4 downto 0);
            i_D0  : in std_logic_vector(31 downto 0);
            i_D1  : in std_logic_vector(31 downto 0);
            i_D2  : in std_logic_vector(31 downto 0);
            i_D3  : in std_logic_vector(31 downto 0);
            i_D4  : in std_logic_vector(31 downto 0);
            i_D5  : in std_logic_vector(31 downto 0);
            i_D6  : in std_logic_vector(31 downto 0);
            i_D7  : in std_logic_vector(31 downto 0);
            i_D8  : in std_logic_vector(31 downto 0);
            i_D9  : in std_logic_vector(31 downto 0);
            i_D10 : in std_logic_vector(31 downto 0);
            i_D11 : in std_logic_vector(31 downto 0);
            i_D12 : in std_logic_vector(31 downto 0);
            i_D13 : in std_logic_vector(31 downto 0);
            i_D14 : in std_logic_vector(31 downto 0);
            i_D15 : in std_logic_vector(31 downto 0);
            i_D16 : in std_logic_vector(31 downto 0);
            i_D17 : in std_logic_vector(31 downto 0);
            i_D18 : in std_logic_vector(31 downto 0);
            i_D19 : in std_logic_vector(31 downto 0);
            i_D20 : in std_logic_vector(31 downto 0);
            i_D21 : in std_logic_vector(31 downto 0);
            i_D22 : in std_logic_vector(31 downto 0);
            i_D23 : in std_logic_vector(31 downto 0);
            i_D24 : in std_logic_vector(31 downto 0);
            i_D25 : in std_logic_vector(31 downto 0);
            i_D26 : in std_logic_vector(31 downto 0);
            i_D27 : in std_logic_vector(31 downto 0);
            i_D28 : in std_logic_vector(31 downto 0);
            i_D29 : in std_logic_vector(31 downto 0);
            i_D30 : in std_logic_vector(31 downto 0);
            i_D31 : in std_logic_vector(31 downto 0);
            o_O   : out std_logic_vector(31 downto 0)
        );
    end component;

    signal reg_write_enables : std_logic_vector(31 downto 0);
    signal reg_outputs       : std_logic_vector_array(31 downto 0);
    signal mux_a_out         : std_logic_vector(31 downto 0) := x"00000000";
    signal mux_b_out         : std_logic_vector(31 downto 0) := x"00000000";
    signal write_address     : std_logic_vector(4 downto 0); -- Write address, will be set to "00000" if write_enable set to 0

begin

    read_port_a_data <= mux_a_out;
    read_port_b_data <= mux_b_out;

    with write_enable select
        write_address <= write_addr when '1',
        "00000" when others;

    write_decoder : decoder_5to32
    port map(
        write_address,
        reg_write_enables
    );

    reg_0 : nbit_register
    generic map(32)
    port map(
        clk,
        '1',
        '0',
        x"00000000",
        reg_outputs(0)
    );

    G_1 : for i in 31 downto 1 generate
        reg_i : nbit_register
        generic map(32)
        port map(
            clk,
            rst,
            reg_write_enables(i),
            write_data,
            reg_outputs(i)
        );
    end generate;

    mux_read_A : mux32to1
    port map(
        read_port_a_addr,
        reg_outputs(0),
        reg_outputs(1),
        reg_outputs(2),
        reg_outputs(3),
        reg_outputs(4),
        reg_outputs(5),
        reg_outputs(6),
        reg_outputs(7),
        reg_outputs(8),
        reg_outputs(9),
        reg_outputs(10),
        reg_outputs(11),
        reg_outputs(12),
        reg_outputs(13),
        reg_outputs(14),
        reg_outputs(15),
        reg_outputs(16),
        reg_outputs(17),
        reg_outputs(18),
        reg_outputs(19),
        reg_outputs(20),
        reg_outputs(21),
        reg_outputs(22),
        reg_outputs(23),
        reg_outputs(24),
        reg_outputs(25),
        reg_outputs(26),
        reg_outputs(27),
        reg_outputs(28),
        reg_outputs(29),
        reg_outputs(30),
        reg_outputs(31),
        mux_a_out
    );

    mux_read_B : mux32to1
    port map(
        read_port_b_addr,
        reg_outputs(0),
        reg_outputs(1),
        reg_outputs(2),
        reg_outputs(3),
        reg_outputs(4),
        reg_outputs(5),
        reg_outputs(6),
        reg_outputs(7),
        reg_outputs(8),
        reg_outputs(9),
        reg_outputs(10),
        reg_outputs(11),
        reg_outputs(12),
        reg_outputs(13),
        reg_outputs(14),
        reg_outputs(15),
        reg_outputs(16),
        reg_outputs(17),
        reg_outputs(18),
        reg_outputs(19),
        reg_outputs(20),
        reg_outputs(21),
        reg_outputs(22),
        reg_outputs(23),
        reg_outputs(24),
        reg_outputs(25),
        reg_outputs(26),
        reg_outputs(27),
        reg_outputs(28),
        reg_outputs(29),
        reg_outputs(30),
        reg_outputs(31),
        mux_b_out
    );

end structural; -- structural
