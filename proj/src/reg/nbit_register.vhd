-------------------------------------------------------------------------
-- Branden Butler
-- CprE 381
-- Iowa State University
-------------------------------------------------------------------------
-- nbit_register.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of an n-bit
-- register using D Flip Flops as a basis.

library IEEE;
use IEEE.std_logic_1164.all;

entity nbit_register is
    generic (N : integer := 16);
    port (
        i_CLK : in std_logic;
        i_RST : in std_logic;
        i_WE  : in std_logic; -- Write enable
        i_D   : in std_logic_vector(N - 1 downto 0);
        o_Q   : out std_logic_vector(N - 1 downto 0)
    );
end nbit_register;

architecture structural of nbit_register is

    component dffg is

        port (
            i_CLK : in std_logic;   -- Clock input
            i_RST : in std_logic;   -- Reset input
            i_WE  : in std_logic;   -- Write enable input
            i_D   : in std_logic;   -- Data value input
            o_Q   : out std_logic); -- Data value output

    end component;

begin

    GNBit_Register : for i in N - 1 downto 0 generate
        DFFI : dffg
        port map(
            i_CLK,
            i_RST,
            i_WE,
            i_D(i),
            o_Q(i)
        );
    end generate;

end structural;
