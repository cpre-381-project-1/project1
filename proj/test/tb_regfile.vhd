-------------------------------------------------------------------------
-- Branden Butler
-- CPRE 38'1' Lab '1'
-- Iowa State University
------------------------------------------------------------------------

-- tb_regfile.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbench for regfile.vhd

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
library std;
use std.env.all;    -- For hierarchical/external signals
use std.textio.all; -- For basic I/O

library work;
use work.MIPS_types.all;

entity tb_regfile is
    generic (gCLK_HPER : time := 10 ns); -- Generic for half of the clock cycle period
end tb_regfile;

architecture mixed of tb_regfile is

    -- Define the total clock period time
    constant cCLK_PER : time := gCLK_HPER * 2;

    -- We will be instantiating our design under test (DUT), so we need to specify its
    -- component interface.
    component regfile is
        port (
            clk              : in std_logic;
            rst              : in std_logic;
            write_addr       : in std_logic_vector(4 downto 0);
            write_data       : in std_logic_vector(31 downto 0);
            read_port_a_addr : in std_logic_vector(4 downto 0);
            read_port_a_data : out std_logic_vector(31 downto 0);
            read_port_b_addr : in std_logic_vector(4 downto 0);
            read_port_b_data : out std_logic_vector(31 downto 0)
        );
    end component;

    -- Create signals for all of the inputs and outputs of the file that you are testing
    -- := ''0'' or := (others => ''0'') just make all the signals start at an initial value of zero
    signal CLK, reset  : std_logic := '0';

    signal s_rst              : std_logic := '0';
    signal s_write_addr       : std_logic_vector(4 downto 0) := "00000";
    signal s_write_data       : std_logic_vector(31 downto 0) := x"00000000";
    signal s_read_port_a_addr : std_logic_vector(4 downto 0) := "00000";
    signal s_read_port_a_data : std_logic_vector(31 downto 0) := x"00000000";
    signal s_read_port_b_addr : std_logic_vector(4 downto 0) := "00000";
    signal s_read_port_b_data : std_logic_vector(31 downto 0) := x"00000000";

begin

    DUT0 : regfile
    port map(
        CLK,
        s_rst,
        s_write_addr,
        s_write_data,
        s_read_port_a_addr,
        s_read_port_a_data,
        s_read_port_b_addr,
        s_read_port_b_data
    );

    --This first process is to setup the clock for the test bench
    P_CLK : process
    begin
        CLK <= '1';         -- clock starts at '1'
        wait for gCLK_HPER; -- after half a cycle
        CLK <= '0';         -- clock becomes a '0' (negative edge)
        wait for gCLK_HPER; -- after half a cycle, process begins evaluation again
    end process;

    -- This process resets the sequential components of the design.
    -- It is held to be '1' across both the negative and positive edges of the clock
    -- so it works regardless of whether the design uses synchronous (pos or neg edge)
    -- or asynchronous resets.
    P_RST : process
    begin
        reset <= '0';
        wait for gCLK_HPER/2;
        reset <= '1';
        wait for gCLK_HPER * 2;
        reset <= '0';
        wait;
    end process;
    -- Assign inputs for each test case.
    -- TODO: add test cases as needed.
    P_TEST_CASES : process
    begin
        wait for gCLK_HPER/2; -- for waveform clarity, I prefer not to change inputs on clk edges

        -- Setup
        s_rst <= '0';
        s_write_addr       <= "00000";
        s_write_data       <= x"00000000";
        s_read_port_a_addr <= "00000";
        s_read_port_b_addr <= "00000";

        -- Test case '1':
        -- Store '1' in $1
        s_write_addr <= "00001";
        s_write_data <= x"00000010";
        wait for gCLK_HPER * 2;
        -- Expect: s_o_O to be zero

        -- Test case 2:
        -- Read $1
        s_write_addr <= "00000";
        s_read_port_b_addr <= "00001";
        wait for gCLK_HPER * 2;
        -- Expect: s_o_O to be 0x10

        -- Test case 3:
        -- 2
        s_write_addr <= "10000";
        s_write_data <= x"00000010";

        wait for gCLK_HPER * 2;

        s_read_port_a_addr <= "10000";
        wait for gCLK_HPER * 2;
        -- Expect: s_o_O to be 0x10

        -- Test case 4:
        -- 3
        s_write_addr <= "00000";
        s_write_data <= x"00000010";
        s_read_port_b_addr <= "00000";
        wait for gCLK_HPER * 2;
        -- Expect: s_o_O to be 3

        -- Test case 5:
        -- 4
        -- s_i_S <= "00100";
        wait for gCLK_HPER * 2;
        -- Expect: s_o_O to be 4

    end process;

end mixed;
