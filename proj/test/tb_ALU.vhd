-------------------------------------------------------------------------
-- Tyler Duquette
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- tb_ALU.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbench for the ALU
--------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all; 
library std;
use std.env.all;                
use std.textio.all;            

entity tb_ALU is
  generic(gCLK_HPER   : time := 10 ns);
end tb_ALU;

architecture mixed of tb_ALU is

-- Define the total clock period time
constant cCLK_PER  : time := gCLK_HPER * 2;


component ALU is
port(inputA    : in std_logic_vector(31 downto 0);
     inputB    : in std_logic_vector(31 downto 0);
     opSelect  : in std_logic_vector(3 downto 0);
     zeroOut   : out std_logic; -- 1 when resultOut = 0
     overflow  : out std_logic;
     resultOut : out std_logic_vector(31 downto 0));
end component;


signal CLK, reset : std_logic := '0';

 
signal s_A  : std_logic_vector(31 downto 0);
signal s_B  : std_logic_vector(31 downto 0);
signal s_OP : std_logic_vector(3 downto 0);
signal s_O  : std_logic_vector(31 downto 0);
signal s_Z  : std_logic;
signal s_Of : std_logic;

begin

  DUT0: ALU
  port map(inputA    => s_A,
           inputB    => s_B,
           opSelect  => s_OP,
           zeroOut   => s_Z,
           overflow  => s_Of,
           resultOut => s_O);
           

  
  --This first process is to setup the clock for the test bench
  P_CLK: process
  begin
    CLK <= '1';         -- clock starts at 1
    wait for gCLK_HPER; -- after half a cycle
    CLK <= '0';         -- clock becomes a 0 (negative edge)
    wait for gCLK_HPER; -- after half a cycle, process begins evaluation again
  end process;


  P_RST: process
  begin
  	reset <= '0';   
    wait for gCLK_HPER/2;
	reset <= '1';
    wait for gCLK_HPER*2;
	reset <= '0';
	wait;
  end process;  
  
  P_TEST_CASES: process
  begin
    wait for gCLK_HPER/2; 

    -- Test case 1 (and):
    s_A <= x"A28B2C9D";
    s_B <= x"FFFFFFFF";
    s_OP <= "0000";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;


    -- Test case 2 (or):
    s_A <= x"02314401";
    s_B <= x"00000000";
    s_OP <= "0001";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;


    -- Test case 3 (add):
    s_A <= x"00000002";
    s_B <= x"00000004";
    s_OP <= "0010";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;


    -- Test case 4 (sub):
    s_A <= x"00000004";
    s_B <= x"00000004";
    s_OP <= "0011";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;


    -- Test case 5 (xor):
    s_A <= x"01010101";
    s_B <= x"10101010";
    s_OP <= "0100";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 6 (nor):
    s_A <= x"1223559A";
    s_B <= x"00000000";
    s_OP <= "0101";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 7 (slt):
    s_A <= x"00000024";
    s_B <= x"00000360";
    s_OP <= "0110";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 8 (sll):
    s_A <= x"00000004";
    s_B <= x"00000002";
    s_OP <= "0111";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 9 (srl):
    s_A <= x"00000010";
    s_B <= x"00000002";
    s_OP <= "1000";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 10 (sra):
    s_A <= x"F1000000";
    s_B <= x"00000008";
    s_OP <= "1001";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 11 (repl):
    s_A <= x"00000024";
    s_B <= x"00000000";
    s_OP <= "1010";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 12 (not):
    s_A <= x"00000000";
    s_B <= x"00000000";
    s_OP <= "1011";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 13 (lui):
    s_A <= x"00000000";
    s_B <= x"00001234";
    s_OP <= "1100";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;
  

    

 
    
  end process;

end mixed;