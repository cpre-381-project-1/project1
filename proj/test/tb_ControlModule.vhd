-------------------------------------------------------------------------
-- Tyler Duquette
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- tb_ControlModule.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbench for the control module
--------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all; 
library std;
use std.env.all;                
use std.textio.all;            

entity tb_ControlModule is
  generic(gCLK_HPER   : time := 10 ns);
end tb_ControlModule;

architecture mixed of tb_ControlModule is

-- Define the total clock period time
constant cCLK_PER  : time := gCLK_HPER * 2;


component controlModule is
port(opCode    : in std_logic_vector(5 downto 0);
     funct     : in std_logic_vector(5 downto 0);
     ALUSrc    : out std_logic;
     ALUOp     : out std_logic_vector(3 downto 0);
     MemtoReg  : out std_logic;
     MemWrite  : out std_logic;
     MemRead   : out std_logic;
     RegWrite  : out std_logic;
     RegDst    : out std_logic_vector(1 downto 0);
     Jump      : out std_logic;
     Branch    : out std_logic;
     Shift     : out std_logic;
     Halt      : out std_logic;
     opSlt     : out std_logic;
     opJal     : out std_logic;
     opJr      : out std_logic;
     opBne     : out std_logic);
end component;


signal CLK, reset : std_logic := '0';

 
signal s_OP    : std_logic_vector(5 downto 0);
signal s_FC    : std_logic_vector(5 downto 0);
signal s_ALUOP : std_logic_vector(3 downto 0);
signal s_Dst   : std_logic_vector(1 downto 0);
signal s_Src   : std_logic;
signal s_Memto : std_logic;
signal s_MemWr : std_logic;
signal s_MemRd : std_logic;
signal s_RegWr : std_logic;
signal s_Jmp   : std_logic;
signal s_Br    : std_logic;
signal s_Hlt   : std_logic;
signal s_Shft  : std_logic;
signal s_slt   : std_logic;
signal s_jal   : std_logic;
signal s_jr    : std_logic;
signal s_bne   : std_logic;

begin

  DUT0: controlModule
  port map(opCode    => s_OP,
           funct     => s_FC,
           ALUSrc    => s_Src,
           ALUOp     => s_ALUOP,
           MemtoReg  => s_Memto,
           MemWrite  => s_MemWr,
           MemRead   => s_MemRd,
           RegWrite  => s_RegWr,
           RegDst    => s_Dst,
           Jump      => s_Jmp,
           Branch    => s_Br,
           Shift     => s_Shft,
           Halt      => s_Hlt,
           opSlt     => s_slt,
           opJal     => s_jal,
           opJr      => s_jr,
           opBne     => s_bne);

  
  --This first process is to setup the clock for the test bench
  P_CLK: process
  begin
    CLK <= '1';         -- clock starts at 1
    wait for gCLK_HPER; -- after half a cycle
    CLK <= '0';         -- clock becomes a 0 (negative edge)
    wait for gCLK_HPER; -- after half a cycle, process begins evaluation again
  end process;


  P_RST: process
  begin
  	reset <= '0';   
    wait for gCLK_HPER/2;
	reset <= '1';
    wait for gCLK_HPER*2;
	reset <= '0';
	wait;
  end process;  
  
  P_TEST_CASES: process
  begin
    wait for gCLK_HPER/2; 

    -- Test case 1 (add):
    s_OP <= "000000";
    s_FC <= "100000";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 2 (nor):
    s_OP <= "000000";
    s_FC <= "100111";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 3 (slt):
    s_OP <= "000000";
    s_FC <= "101010";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 4 (jal):
    s_OP <= "000011";
    s_FC <= "000000";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 5 (repl):
    s_OP <= "101000";
    s_FC <= "111111";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 6 (lui):
    s_OP <= "001111";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 7 (sw):
    s_OP <= "101011";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    

 
    
  end process;

end mixed;