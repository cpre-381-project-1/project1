-------------------------------------------------------------------------
-- Branden Butler
-- CPRE 38'1' Lab '1'
-- Iowa State University
------------------------------------------------------------------------

-- tb_mux2to1.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbench for mux2to1.vhd

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O

entity  tb_mux2to1_N is
  generic(gCLK_HPER   : time := 10 ns);   -- Generic for half of the clock cycle period
end tb_mux2to1_N;

architecture mixed of tb_mux2to1_N is

	-- Define the total clock period time
	constant cCLK_PER  : time := gCLK_HPER * 2;

	-- We will be instantiating our design under test (DUT), so we need to specify its
	-- component interface.

	component mux2to1_N is
		generic(N : integer := 16); -- Generic of type integer for input/output data width.
		port(
			i_S	: in 	std_logic;
			i_D0	: in 	std_logic_vector(N-1 downto 0);
			i_D1	: in 	std_logic_vector(N-1 downto 0);
			o_O	: out 	std_logic_vector(N-1 downto 0)
		);
	end component;

	-- Create signals for all of the inputs and outputs of the file that you are testing
	-- := ''0'' or := (others => ''0'') just make all the signals start at an initial value of zero
	signal CLK, reset : std_logic := '0';

	signal s_i_S	: std_logic := '0';
	signal s_i_D0	: std_logic_vector(1 downto 0) := "00";
	signal s_i_D1	: std_logic_vector(1 downto 0) := "00";
	signal s_o_O	: std_logic_vector(1 downto 0) := "00";

	begin
		
		DUT0: mux2to1_N
		generic map (2)
		port map(
			s_i_S,
			s_i_D0,
			s_i_D1,
			s_o_O
		);

		--This first process is to setup the clock for the test bench
		P_CLK: process
		begin
			CLK <= '1';         -- clock starts at '1'
			wait for gCLK_HPER; -- after half a cycle
			CLK <= '0';         -- clock becomes a '0' (negative edge)
			wait for gCLK_HPER; -- after half a cycle, process begins evaluation again
		end process;

		-- This process resets the sequential components of the design.
		-- It is held to be '1' across both the negative and positive edges of the clock
		-- so it works regardless of whether the design uses synchronous (pos or neg edge)
		-- or asynchronous resets.
		P_RST: process
		begin
			reset <= '0';   
			wait for gCLK_HPER/2;
			reset <= '1';
			wait for gCLK_HPER*2;
			reset <= '0';
			wait;
		end process;


		-- Assign inputs for each test case.
		-- TODO: add test cases as needed.
		P_TEST_CASES: process
		begin
			wait for gCLK_HPER/2; -- for waveform clarity, I prefer not to change inputs on clk edges

			-- Test case '1':
			-- All zero
			s_i_S   <= '0';
			s_i_D0   <= "00";
			s_i_D1 <= "00";
			wait for gCLK_HPER*2;
			-- Expect: s_o_O to be zero after positive edge of clock

			-- Test case 2:
			-- s_i_D0 "01" and s_i_S zero
			s_i_S   <= '0';
			s_i_D0   <= "01";
			s_i_D1 <= "00";
			wait for gCLK_HPER*2;
			-- Expect: s_o_O to be '1'

			-- Test case 3:
			-- s_i_D1 "01" and s_i_S '0'
			s_i_S   <= '0';
			s_i_D0   <= "00";
			s_i_D1 <= "01";

			wait for gCLK_HPER*2;
			-- Expect: s_o_O to be zero

			-- Test case 4:
			-- s_i_D1 and D0 "01" and s_i_S '0'
			s_i_S   <= '0';
			s_i_D0   <= "01";
			s_i_D1 <= "01";

			wait for gCLK_HPER*2;
			-- Expect: s_o_O to be '1'

			-- Test case 5:
			-- s_i_D1 and D0 '0' and s_i_S '1'
			s_i_S   <= '1';
			s_i_D0   <= "00";
			s_i_D1 <= "00";

			wait for gCLK_HPER*2;
			-- Expect: s_o_O to be zero

			-- Test case 6:
			-- s_i_D0 '1' and s_i_S '1'
			s_i_S   <= '1';
			s_i_D0   <= "01";
			s_i_D1 <= "00";

			wait for gCLK_HPER*2;
			-- Expect: s_o_O to be zero

			-- Test case 7:
			-- s_i_D1 '1' and s_i_S '1'
			s_i_S   <= '1';
			s_i_D0   <= "00";
			s_i_D1 <= "01";

			wait for gCLK_HPER*2;
			-- Expect: s_o_O to be '1'

			-- Test case 8:
			-- s_i_D1 and D'0' '1' and s_i_S '1'
			s_i_S   <= '1';
			s_i_D0   <= "01";
			s_i_D1 <= "01";

			wait for gCLK_HPER*2;
			-- Expect: s_o_O to be '1'

			-- Test case 9:
			-- s_i_D0 '10' and s_i_S '0'
			s_i_S   <= '0';
			s_i_D0   <= "10";
			s_i_D1 <= "00";

			wait for gCLK_HPER*2;
			-- Expect: s_o_O to be '1'

			-- Test case 10:
			-- s_i_D1 '11' and s_i_S '1'
			s_i_S   <= '1';
			s_i_D0   <= "00";
			s_i_D1 <= "11";

			wait for gCLK_HPER*2;
			-- Expect: s_o_O to be '1'

			
		end process;



end mixed;