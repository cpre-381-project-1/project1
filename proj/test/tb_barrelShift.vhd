-------------------------------------------------------------------------
-- Tyler Duquette
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- tb_barrelShift.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbench for a barrel shifter
--------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all; 
library std;
use std.env.all;                
use std.textio.all;            

entity tb_barrelShift is
  generic(gCLK_HPER   : time := 10 ns);
end tb_barrelShift;

architecture mixed of tb_barrelShift is

-- Define the total clock period time
constant cCLK_PER  : time := gCLK_HPER * 2;


component barrelShifter is
port(shiftIn	: in std_logic_vector(31 downto 0);
     shiftMd    : in std_logic; --0 for Logical/1 for Arithmetic
     shiftDr    : in std_logic; --0 for Right/1 for Left
     shiftNm    : in std_logic_vector(4 downto 0);
     shiftOt    : out std_logic_vector(31 downto 0));
end component;


signal CLK, reset : std_logic := '0';

signal s_In   : std_logic_vector(31 downto 0);
signal s_Md   : std_logic := '0';
signal s_Dr   : std_logic := '0';
signal s_Nm   : std_logic_vector(4 downto 0);
signal s_Ot   : std_logic_vector(31 downto 0);

begin

  DUT0: barrelShifter
  port map(shiftIn  => s_In,
           shiftMd  => s_Md,
           shiftDr  => s_Dr,
           shiftNm  => s_Nm,
           shiftOt  => s_Ot);

  
  --This first process is to setup the clock for the test bench
  P_CLK: process
  begin
    CLK <= '1';         -- clock starts at 1
    wait for gCLK_HPER; -- after half a cycle
    CLK <= '0';         -- clock becomes a 0 (negative edge)
    wait for gCLK_HPER; -- after half a cycle, process begins evaluation again
  end process;


  P_RST: process
  begin
  	reset <= '0';   
    wait for gCLK_HPER/2;
	reset <= '1';
    wait for gCLK_HPER*2;
	reset <= '0';
	wait;
  end process;  
  
  P_TEST_CASES: process
  begin
    wait for gCLK_HPER/2; 

    -- Test case 1:
    s_In <= x"00200000";
    s_Nm <= "00011";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 2:
    s_Nm <= "00010";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 3 (Left):
    s_Dr <= '1';
    s_Nm <= "00001";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

    -- Test case 4 (Arithmetic):
    s_In <= x"F0001000";
    s_Md <= '1';
    s_Dr <= '0';
    s_Nm <= "00010";
    wait for gCLK_HPER*2;
    wait for gCLK_HPER*2;

 
    
  end process;

end mixed;