-------------------------------------------------------------------------
-- Branden Butler
-- CPRE 38'1' Lab '1'
-- Iowa State University
------------------------------------------------------------------------

-- tb_mux332to1.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbench for mux32to1.vhd

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
library std;
use std.env.all;    -- For hierarchical/external signals
use std.textio.all; -- For basic I/O

entity tb_mux32to1 is
	generic (gCLK_HPER : time := 10 ns); -- Generic for half of the clock cycle period
end tb_mux32to1;

architecture mixed of tb_mux32to1 is

	-- Define the total clock period time
	constant cCLK_PER : time := gCLK_HPER * 2;

	-- We will be instantiating our design under test (DUT), so we need to specify its
	-- component interface.

	component mux32to1 is
		port (
			i_S   : in std_logic_vector(4 downto 0);
			i_D0  : in std_logic_vector(31 downto 0);
			i_D1  : in std_logic_vector(31 downto 0);
			i_D2  : in std_logic_vector(31 downto 0);
			i_D3  : in std_logic_vector(31 downto 0);
			i_D4  : in std_logic_vector(31 downto 0);
			i_D5  : in std_logic_vector(31 downto 0);
			i_D6  : in std_logic_vector(31 downto 0);
			i_D7  : in std_logic_vector(31 downto 0);
			i_D8  : in std_logic_vector(31 downto 0);
			i_D9  : in std_logic_vector(31 downto 0);
			i_D10 : in std_logic_vector(31 downto 0);
			i_D11 : in std_logic_vector(31 downto 0);
			i_D12 : in std_logic_vector(31 downto 0);
			i_D13 : in std_logic_vector(31 downto 0);
			i_D14 : in std_logic_vector(31 downto 0);
			i_D15 : in std_logic_vector(31 downto 0);
			i_D16 : in std_logic_vector(31 downto 0);
			i_D17 : in std_logic_vector(31 downto 0);
			i_D18 : in std_logic_vector(31 downto 0);
			i_D19 : in std_logic_vector(31 downto 0);
			i_D20 : in std_logic_vector(31 downto 0);
			i_D21 : in std_logic_vector(31 downto 0);
			i_D22 : in std_logic_vector(31 downto 0);
			i_D23 : in std_logic_vector(31 downto 0);
			i_D24 : in std_logic_vector(31 downto 0);
			i_D25 : in std_logic_vector(31 downto 0);
			i_D26 : in std_logic_vector(31 downto 0);
			i_D27 : in std_logic_vector(31 downto 0);
			i_D28 : in std_logic_vector(31 downto 0);
			i_D29 : in std_logic_vector(31 downto 0);
			i_D30 : in std_logic_vector(31 downto 0);
			i_D31 : in std_logic_vector(31 downto 0);
			o_O   : out std_logic_vector(31 downto 0)
		);
	end component;

	-- Create signals for all of the inputs and outputs of the file that you are testing
	-- := ''0'' or := (others => ''0'') just make all the signals start at an initial value of zero
	signal CLK, reset : std_logic                     := '0';

	signal s_i_S      : std_logic_vector(4 downto 0)  := "00000";
	signal s_i_D0     : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D1     : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D2     : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D3     : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D4     : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D5     : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D6     : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D7     : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D8     : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D9     : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D10    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D11    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D12    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D13    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D14    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D15    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D16    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D17    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D18    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D19    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D20    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D21    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D22    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D23    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D24    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D25    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D26    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D27    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D28    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D29    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D30    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_i_D31    : std_logic_vector(31 downto 0) := x"00000000";
	signal s_o_O      : std_logic_vector(31 downto 0) := x"00000000";

begin

	DUT0 : mux32to1
	port map(
		s_i_S,
		s_i_D0,
		s_i_D1,
		s_i_D2,
		s_i_D3,
		s_i_D4,
		s_i_D5,
		s_i_D6,
		s_i_D7,
		s_i_D8,
		s_i_D9,
		s_i_D10,
		s_i_D11,
		s_i_D12,
		s_i_D13,
		s_i_D14,
		s_i_D15,
		s_i_D16,
		s_i_D17,
		s_i_D18,
		s_i_D19,
		s_i_D20,
		s_i_D21,
		s_i_D22,
		s_i_D23,
		s_i_D24,
		s_i_D25,
		s_i_D26,
		s_i_D27,
		s_i_D28,
		s_i_D29,
		s_i_D30,
		s_i_D31,
		s_o_O

	);

	--This first process is to setup the clock for the test bench
	P_CLK : process
	begin
		CLK <= '1';         -- clock starts at '1'
		wait for gCLK_HPER; -- after half a cycle
		CLK <= '0';         -- clock becomes a '0' (negative edge)
		wait for gCLK_HPER; -- after half a cycle, process begins evaluation again
	end process;

	-- This process resets the sequential components of the design.
	-- It is held to be '1' across both the negative and positive edges of the clock
	-- so it works regardless of whether the design uses synchronous (pos or neg edge)
	-- or asynchronous resets.
	P_RST : process
	begin
		reset <= '0';
		wait for gCLK_HPER/2;
		reset <= '1';
		wait for gCLK_HPER * 2;
		reset <= '0';
		wait;
	end process;
	-- Assign inputs for each test case.
	-- TODO: add test cases as needed.
	P_TEST_CASES : process
	begin
		wait for gCLK_HPER/2; -- for waveform clarity, I prefer not to change inputs on clk edges

		-- Setup
		s_i_D0 <= x"00000000";
		s_i_D1 <= x"00000001";
		s_i_D2 <= x"00000002";
		s_i_D3 <= x"00000003";
		s_i_D4 <= x"00000004";

		-- Test case '1':
		-- All zero
		s_i_S  <= "00000";
		wait for gCLK_HPER * 2;
		-- Expect: s_o_O to be zero

		-- Test case 2:
		-- 1
		s_i_S <= "00001";
		wait for gCLK_HPER * 2;
		-- Expect: s_o_O to be 1

		-- Test case 3:
		-- 2
		s_i_S <= "00010";
		wait for gCLK_HPER * 2;
		-- Expect: s_o_O to be 2

		-- Test case 4:
		-- 3
		s_i_S <= "00011";
		wait for gCLK_HPER * 2;
		-- Expect: s_o_O to be 3

		-- Test case 5:
		-- 4
		s_i_S <= "00100";
		wait for gCLK_HPER * 2;
		-- Expect: s_o_O to be 4

	end process;

end mixed;
