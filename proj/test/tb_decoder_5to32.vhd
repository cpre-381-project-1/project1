-------------------------------------------------------------------------
-- Branden Butler
-- CprE 381
-- Iowa State University
-------------------------------------------------------------------------


-- tb_decoder_5to32.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a simple VHDL testbench for the
-- 5:32 register

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_decoder_5to32 is
  generic(gCLK_HPER   : time := 50 ns);
end tb_decoder_5to32;

architecture behavior of tb_decoder_5to32 is
  
  -- Calculate the clock period as twice the half-period
  constant cCLK_PER  : time := gCLK_HPER * 2;


  component decoder_5to32 is
    port (
        X : in std_logic_vector(4 downto 0);
        Y : out std_logic_vector(31 downto 0)
    );
end component;

  -- Temporary signals to connect to the dff component.
  signal s_CLK, s_RST, s_WE  : std_logic;
  signal s_X: std_logic_vector(4 downto 0) := "00000";
  signal s_Y: std_logic_vector(31 downto 0) := x"00000000";

begin

  DUT: decoder_5to32
  port map(s_X, s_Y);

  -- This process sets the clock value (low for gCLK_HPER, then high
  -- for gCLK_HPER). Absent a "wait" command, processes restart 
  -- at the beginning once they have reached the final statement.
  P_CLK: process
  begin
    s_CLK <= '0';
    wait for gCLK_HPER;
    s_CLK <= '1';
    wait for gCLK_HPER;
  end process;
  
  -- Testbench process  
  P_TB: process
  begin
    -- Reset the FF
    s_RST <= '1';
    s_WE  <= '0';
    s_X   <= "00000";
    wait for cCLK_PER;

    -- Test Case 1
    s_RST <= '0';
    s_WE  <= '1';
    s_X   <= "00000";
    wait for cCLK_PER;
    -- Expect: 1 as output

    -- Test case 2
    -- 1 as input
    s_X   <= "00001";
    wait for cCLK_PER; 
    -- Expect: 2 as output
    
    -- Test case 3
    -- 2 as input
    s_X   <= "00010";
    wait for cCLK_PER; 
    -- Expect: 4 as output

    -- Test case 4
    -- 3 as input
    s_X   <= "00011";
    wait for cCLK_PER; 
    -- Expect: 8 as output

    -- Test case 5
    -- 4 as input
    s_X   <= "00100";
    wait for cCLK_PER; 
    -- Expect: 16 as output

    wait;
  end process;
  
end behavior;
