-------------------------------------------------------------------------
-- Branden Butler
-- CPRE 381 Lab 1
-- Iowa State University
------------------------------------------------------------------------

-- tb_fetch.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbench for fetch.vhd

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
library std;
use std.env.all;    -- For hierarchical/external signals
use std.textio.all; -- For basic I/O

entity tb_fetch is
    generic (gCLK_HPER : time := 10 ns); -- Generic for half of the clock cycle period
end tb_fetch;

architecture mixed of tb_fetch is

    -- Define the total clock period time
    constant cCLK_PER : time := gCLK_HPER * 2;

    -- We will be instantiating our design under test (DUT), so we need to specify its
    -- component interface.

    component fetch is
        port (
            clk              : in std_logic;                      -- Clock signal
            jmp              : in std_logic;                      -- Whether to jump
            jr               : in std_logic;                      -- Whether to jump with register input
            jmp_reg          : in std_logic_vector(31 downto 0);  -- The jump address from a register if jr
            sign_extend_imm  : in std_logic_vector(31 downto 0);  -- Sign extension of last 16 bits of instruction for j format
            branch           : in std_logic;                      -- Whether we're executing a branch instruction
            zero             : in std_logic;                      -- Whether the ALU output 0
            bne              : in std_logic;                      -- Whether the executed branch instruction is bne
            instr            : in std_logic_vector(31 downto 0);  -- current instruction
            rst              : in std_logic;                      -- Reset PC to a given address
            rst_addr         : in std_logic_vector(31 downto 0);  -- The address to reset PC to
            next_instr_addr  : out std_logic_vector(31 downto 0); -- The address of the next instruction
            pc_plus_4_output : out std_logic_vector(31 downto 0)
        );
    end component;

    -- Create signals for all of the inputs and outputs of the file that you are testing
    -- := ''0'' or := (others => ''0'') just make all the signals start at an initial value of zero
    signal CLK, reset         : std_logic                     := '0';

    signal s_jmp              : std_logic                     := '0';
    signal s_jr               : std_logic                     := '0';
    signal s_jmp_reg          : std_logic_vector(31 downto 0) := x"00000000";
    signal s_sign_extend_imm  : std_logic_vector(31 downto 0) := x"00000000";
    signal s_branch           : std_logic                     := '0';
    signal s_zero             : std_logic                     := '0';
    signal s_bne              : std_logic                     := '0';
    signal s_instr            : std_logic_vector(31 downto 0) := x"00000000";
    signal s_rst              : std_logic                     := '0';
    signal s_rst_addr         : std_logic_vector(31 downto 0) := x"00000000";
    signal s_next_instr_addr  : std_logic_vector(31 downto 0) := x"00000000";
    signal s_pc_plus_4_output : std_logic_vector(31 downto 0) := x"00000000";

begin

    DUT0 : fetch
    port map(
        CLK,
        s_jmp,
        s_jr,
        s_jmp_reg,
        s_sign_extend_imm,
        s_branch,
        s_zero,
        s_bne,
        s_instr,
        s_rst,
        s_rst_addr,
        s_next_instr_addr,
        s_pc_plus_4_output
    );

    --This first process is to setup the clock for the test bench
    P_CLK : process
    begin
        CLK <= '1';         -- clock starts at '1'
        wait for gCLK_HPER; -- after half a cycle
        CLK <= '0';         -- clock becomes a '0' (negative edge)
        wait for gCLK_HPER; -- after half a cycle, process begins evaluation again
    end process;

    -- This process resets the sequential components of the design.
    -- It is held to be '1' across both the negative and positive edges of the clock
    -- so it works regardless of whether the design uses synchronous (pos or neg edge)
    -- or asynchronous resets.
    P_RST : process
    begin
        reset <= '0';
        wait for gCLK_HPER/2;
        reset <= '1';
        wait for gCLK_HPER * 2;
        reset <= '0';
        wait;
    end process;
    -- Assign inputs for each test case.
    -- TODO: add test cases as needed.
    P_TEST_CASES : process
    begin
        wait for gCLK_HPER/2; -- for waveform clarity, I prefer not to change inputs on clk edges

        -- Test case '1':
        -- Reset with 0x00000004
        s_jmp             <= '0';
        s_jr              <= '0';
        s_jmp_reg         <= x"00000000";
        s_sign_extend_imm <= x"00000000";
        s_branch          <= '0';
        s_zero            <= '0';
        s_bne             <= '0';
        s_instr           <= x"00000000";
        s_rst             <= '1';
        s_rst_addr        <= x"00000004";
        wait for gCLK_HPER * 2;
        -- Expect: s_next_instr_addr to be 0x00000004 and s_pc_plus_4_output to be 0x00000008
        -- Test case 2: regular PC+4 increment
        s_rst             <= '0';
        s_branch          <= '0';
        s_zero            <= '0';
        s_bne             <= '0';
        s_jmp             <= '0';

        wait for gCLK_HPER * 2;
        -- Expect: s_next_instr_addr to be 0x00000008


        -- Test case 3
        -- beq and branch taken to PC+4 + 4
        s_rst             <= '0';
        s_branch          <= '1';
        s_zero            <= '1';
        s_bne             <= '0';
        s_jmp             <= '0';
        s_sign_extend_imm <= x"00000001";

        wait for gCLK_HPER * 2;
        -- Expect: s_next_instr_addr to be 0x00000010 and s_pc_plus_4_output to be 0x00000014

        -- Test case 4: Jump with register value 0x0000FFFF
        s_jr <= '1';
        s_jmp_reg <= x"0000FFFF";

        wait for gCLK_HPER * 2;
        -- Expect: s_next_instr_addr = 0x0000FFFF

        -- Test Case 5: j with 0x3FFFFFF
        s_instr <= "000010" & "11" & x"FFFFFF";
        s_jmp <= '1';
        s_jr <= '0';

        wait for gCLK_HPER * 2;

        -- Expect 0x0FFFFFFC

        wait for gCLK_HPER * 2;

    end process;

end mixed;
