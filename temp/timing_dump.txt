Info (125069): Default assignment values were changed in the current version of the Quartus Prime software -- changes to default assignments values are contained in file /usr/local/quartus/21.1/quartus/linux64/assignment_defaults.qdf
Info: *******************************************************************
Info: Running Quartus Prime Timing Analyzer
    Info: Version 21.1.1 Build 850 06/23/2022 SJ Standard Edition
    Info: Copyright (C) 2022  Intel Corporation. All rights reserved.
    Info: Your use of Intel Corporation's design tools, logic functions 
    Info: and other software and tools, and any partner logic 
    Info: functions, and any output files from any of the foregoing 
    Info: (including device programming or simulation files), and any 
    Info: associated documentation or information are expressly subject 
    Info: to the terms and conditions of the Intel Program License 
    Info: Subscription Agreement, the Intel Quartus Prime License Agreement,
    Info: the Intel FPGA IP License Agreement, or other applicable license
    Info: agreement, including, without limitation, that your use is for
    Info: the sole purpose of programming logic devices manufactured by
    Info: Intel and sold by Intel or its authorized distributors.  Please
    Info: refer to the applicable agreement for further details, at
    Info: https://fpgasoftware.intel.com/eula.
    Info: Processing started: Tue Nov 15 19:47:19 2022
Info: Command: quartus_sta --sdc=toolflow.sdc toolflow --do_report_timing
Info: qsta_default_script.tcl version: #1
Warning (18236): Number of processors has not been specified which may cause overloading on shared machines.  Set the global assignment NUM_PARALLEL_PROCESSORS in your QSF to an appropriate value for best performance.
Info (20030): Parallel compilation is enabled and will use 12 of the 12 processors detected
Info (21076): High junction temperature operating condition is not set. Assuming a default value of '85'.
Info (21076): Low junction temperature operating condition is not set. Assuming a default value of '0'.
Info (332104): Reading SDC File: 'toolflow.sdc'
Info: Found TIMING_ANALYZER_REPORT_SCRIPT_INCLUDE_DEFAULT_ANALYSIS = ON
Info: Analyzing Slow 1200mV 85C Model
Critical Warning (332148): Timing requirements not met
    Info (11105): For recommendations on closing timing, run Report Timing Closure Recommendations in the Timing Analyzer.
Info (332146): Worst-case setup slack is -22.542
    Info (332119):     Slack       End Point TNS Clock 
    Info (332119): ========= =================== =====================
    Info (332119):   -22.542         -510101.484 iCLK 
Info (332146): Worst-case hold slack is 0.402
    Info (332119):     Slack       End Point TNS Clock 
    Info (332119): ========= =================== =====================
    Info (332119):     0.402               0.000 iCLK 
Info (332140): No Recovery paths to report
Info (332140): No Removal paths to report
Info (332146): Worst-case minimum pulse width slack is 9.738
    Info (332119):     Slack       End Point TNS Clock 
    Info (332119): ========= =================== =====================
    Info (332119):     9.738               0.000 iCLK 
Info (332115): Report Timing: Found 1 setup paths (1 violated).  Worst case slack is -22.542
    Info (332115): -to_clock [get_clocks {iCLK}]
    Info (332115): -setup
    Info (332115): -stdout
Info (332115): Path #1: Setup slack is -22.542 (VIOLATED)
    Info (332115): ===================================================================
    Info (332115): From Node    : fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:3:DFFI|s_Q
    Info (332115): To Node      : regfile:registers|nbit_register:\G_1:2:reg_i|dffg:\GNBit_Register:15:DFFI|s_Q
    Info (332115): Launch Clock : iCLK
    Info (332115): Latch Clock  : iCLK
    Info (332115): Data Arrival Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):      0.000      0.000           launch edge time
    Info (332115):      3.093      3.093  R        clock network delay
    Info (332115):      3.325      0.232     uTco  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:3:DFFI|s_Q
    Info (332115):      3.325      0.000 FF  CELL  fetch_unit|PC|\GNBit_Register:3:DFFI|s_Q|q
    Info (332115):      3.716      0.391 FF    IC  s_IMemAddr[3]~1|datad
    Info (332115):      3.841      0.125 FF  CELL  s_IMemAddr[3]~1|combout
    Info (332115):      6.380      2.539 FF    IC  IMem|ram~43599|datab
    Info (332115):      6.803      0.423 FR  CELL  IMem|ram~43599|combout
    Info (332115):      7.009      0.206 RR    IC  IMem|ram~43600|datad
    Info (332115):      7.164      0.155 RR  CELL  IMem|ram~43600|combout
    Info (332115):      8.434      1.270 RR    IC  IMem|ram~43603|datad
    Info (332115):      8.589      0.155 RR  CELL  IMem|ram~43603|combout
    Info (332115):      8.793      0.204 RR    IC  IMem|ram~43606|datad
    Info (332115):      8.948      0.155 RR  CELL  IMem|ram~43606|combout
    Info (332115):      9.676      0.728 RR    IC  IMem|ram~43638|datac
    Info (332115):      9.963      0.287 RR  CELL  IMem|ram~43638|combout
    Info (332115):     10.168      0.205 RR    IC  IMem|ram~43681|datad
    Info (332115):     10.323      0.155 RR  CELL  IMem|ram~43681|combout
    Info (332115):     14.459      4.136 RR    IC  IMem|ram~43724|datad
    Info (332115):     14.614      0.155 RR  CELL  IMem|ram~43724|combout
    Info (332115):     14.819      0.205 RR    IC  IMem|ram~43725|datad
    Info (332115):     14.974      0.155 RR  CELL  IMem|ram~43725|combout
    Info (332115):     18.021      3.047 RR    IC  registers|mux_read_B|Mux12~9|datad
    Info (332115):     18.160      0.139 RF  CELL  registers|mux_read_B|Mux12~9|combout
    Info (332115):     18.389      0.229 FF    IC  registers|mux_read_B|Mux12~10|datad
    Info (332115):     18.539      0.150 FR  CELL  registers|mux_read_B|Mux12~10|combout
    Info (332115):     22.567      4.028 RR    IC  registers|mux_read_B|Mux12~11|dataa
    Info (332115):     22.906      0.339 RR  CELL  registers|mux_read_B|Mux12~11|combout
    Info (332115):     23.111      0.205 RR    IC  registers|mux_read_B|Mux12~16|datad
    Info (332115):     23.266      0.155 RR  CELL  registers|mux_read_B|Mux12~16|combout
    Info (332115):     23.469      0.203 RR    IC  registers|mux_read_B|Mux12~19|datad
    Info (332115):     23.608      0.139 RF  CELL  registers|mux_read_B|Mux12~19|combout
    Info (332115):     24.457      0.849 FF    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~0|datab
    Info (332115):     24.850      0.393 FF  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~0|combout
    Info (332115):     25.094      0.244 FF    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~1|datac
    Info (332115):     25.374      0.280 FF  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~1|combout
    Info (332115):     26.103      0.729 FF    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~0|datac
    Info (332115):     26.383      0.280 FF  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~0|combout
    Info (332115):     26.615      0.232 FF    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~1|datac
    Info (332115):     26.896      0.281 FF  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~1|combout
    Info (332115):     27.642      0.746 FF    IC  arithmetic_logic_unit|Mux27~0|datad
    Info (332115):     27.767      0.125 FF  CELL  arithmetic_logic_unit|Mux27~0|combout
    Info (332115):     27.996      0.229 FF    IC  arithmetic_logic_unit|Mux27~1|datad
    Info (332115):     28.146      0.150 FR  CELL  arithmetic_logic_unit|Mux27~1|combout
    Info (332115):     28.349      0.203 RR    IC  arithmetic_logic_unit|Mux27~2|datad
    Info (332115):     28.504      0.155 RR  CELL  arithmetic_logic_unit|Mux27~2|combout
    Info (332115):     28.709      0.205 RR    IC  arithmetic_logic_unit|Mux27~3|datad
    Info (332115):     28.864      0.155 RR  CELL  arithmetic_logic_unit|Mux27~3|combout
    Info (332115):     29.069      0.205 RR    IC  arithmetic_logic_unit|Mux27~4|datad
    Info (332115):     29.224      0.155 RR  CELL  arithmetic_logic_unit|Mux27~4|combout
    Info (332115):     29.427      0.203 RR    IC  arithmetic_logic_unit|Mux27~5|datad
    Info (332115):     29.582      0.155 RR  CELL  arithmetic_logic_unit|Mux27~5|combout
    Info (332115):     29.786      0.204 RR    IC  arithmetic_logic_unit|Mux27~6|datad
    Info (332115):     29.941      0.155 RR  CELL  arithmetic_logic_unit|Mux27~6|combout
    Info (332115):     30.145      0.204 RR    IC  arithmetic_logic_unit|Mux27~7|datad
    Info (332115):     30.284      0.139 RF  CELL  arithmetic_logic_unit|Mux27~7|combout
    Info (332115):     30.511      0.227 FF    IC  arithmetic_logic_unit|Mux27~8|datad
    Info (332115):     30.636      0.125 FF  CELL  arithmetic_logic_unit|Mux27~8|combout
    Info (332115):     33.257      2.621 FF    IC  DMem|ram~46153|datab
    Info (332115):     33.680      0.423 FR  CELL  DMem|ram~46153|combout
    Info (332115):     33.884      0.204 RR    IC  DMem|ram~46154|datad
    Info (332115):     34.039      0.155 RR  CELL  DMem|ram~46154|combout
    Info (332115):     34.407      0.368 RR    IC  DMem|ram~46157|datac
    Info (332115):     34.694      0.287 RR  CELL  DMem|ram~46157|combout
    Info (332115):     34.927      0.233 RR    IC  DMem|ram~46160|datab
    Info (332115):     35.361      0.434 RF  CELL  DMem|ram~46160|combout
    Info (332115):     38.961      3.600 FF    IC  DMem|ram~46192|datab
    Info (332115):     39.386      0.425 FF  CELL  DMem|ram~46192|combout
    Info (332115):     39.612      0.226 FF    IC  DMem|ram~46235|datad
    Info (332115):     39.737      0.125 FF  CELL  DMem|ram~46235|combout
    Info (332115):     39.964      0.227 FF    IC  DMem|ram~46278|datad
    Info (332115):     40.114      0.150 FR  CELL  DMem|ram~46278|combout
    Info (332115):     41.100      0.986 RR    IC  DMem|ram~46279|datac
    Info (332115):     41.387      0.287 RR  CELL  DMem|ram~46279|combout
    Info (332115):     41.621      0.234 RR    IC  DMem|ram~46450|datab
    Info (332115):     42.039      0.418 RR  CELL  DMem|ram~46450|combout
    Info (332115):     42.244      0.205 RR    IC  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~2|datad
    Info (332115):     42.399      0.155 RR  CELL  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~2|combout
    Info (332115):     42.602      0.203 RR    IC  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~3|datad
    Info (332115):     42.757      0.155 RR  CELL  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~3|combout
    Info (332115):     45.097      2.340 RR    IC  registers|\G_1:2:reg_i|\GNBit_Register:15:DFFI|s_Q|asdata
    Info (332115):     45.503      0.406 RR  CELL  regfile:registers|nbit_register:\G_1:2:reg_i|dffg:\GNBit_Register:15:DFFI|s_Q
    Info (332115): Data Required Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):     20.000     20.000           latch edge time
    Info (332115):     22.955      2.955  R        clock network delay
    Info (332115):     22.963      0.008           clock pessimism removed
    Info (332115):     22.943     -0.020           clock uncertainty
    Info (332115):     22.961      0.018     uTsu  regfile:registers|nbit_register:\G_1:2:reg_i|dffg:\GNBit_Register:15:DFFI|s_Q
    Info (332115): Data Arrival Time  :    45.503
    Info (332115): Data Required Time :    22.961
    Info (332115): Slack              :   -22.542 (VIOLATED)
    Info (332115): ===================================================================
Info (332115): Report Timing: Found 1 hold paths (0 violated).  Worst case slack is 0.402
    Info (332115): -to_clock [get_clocks {iCLK}]
    Info (332115): -hold
    Info (332115): -stdout
Info (332115): Path #1: Hold slack is 0.402 
    Info (332115): ===================================================================
    Info (332115): From Node    : fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): To Node      : fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): Launch Clock : iCLK
    Info (332115): Latch Clock  : iCLK
    Info (332115): Data Arrival Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):      0.000      0.000           launch edge time
    Info (332115):      2.972      2.972  R        clock network delay
    Info (332115):      3.204      0.232     uTco  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115):      3.204      0.000 FF  CELL  fetch_unit|PC|\GNBit_Register:2:DFFI|s_Q|q
    Info (332115):      3.204      0.000 FF    IC  fetch_unit|pc_rst_mux|\G_NBit_MUX:2:MUXI|AND1|o_F~2|datac
    Info (332115):      3.565      0.361 FF  CELL  fetch_unit|pc_rst_mux|\G_NBit_MUX:2:MUXI|AND1|o_F~2|combout
    Info (332115):      3.565      0.000 FF    IC  fetch_unit|PC|\GNBit_Register:2:DFFI|s_Q|d
    Info (332115):      3.641      0.076 FF  CELL  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): Data Required Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):      0.000      0.000           latch edge time
    Info (332115):      3.085      3.085  R        clock network delay
    Info (332115):      3.053     -0.032           clock pessimism removed
    Info (332115):      3.053      0.000           clock uncertainty
    Info (332115):      3.239      0.186      uTh  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): Data Arrival Time  :     3.641
    Info (332115): Data Required Time :     3.239
    Info (332115): Slack              :     0.402 
    Info (332115): ===================================================================
Info: Analyzing Slow 1200mV 0C Model
Info (334003): Started post-fitting delay annotation
Info (334004): Delay annotation completed successfully
Critical Warning (332148): Timing requirements not met
    Info (11105): For recommendations on closing timing, run Report Timing Closure Recommendations in the Timing Analyzer.
Info (332146): Worst-case setup slack is -19.255
    Info (332119):     Slack       End Point TNS Clock 
    Info (332119): ========= =================== =====================
    Info (332119):   -19.255         -420932.808 iCLK 
Info (332146): Worst-case hold slack is 0.355
    Info (332119):     Slack       End Point TNS Clock 
    Info (332119): ========= =================== =====================
    Info (332119):     0.355               0.000 iCLK 
Info (332140): No Recovery paths to report
Info (332140): No Removal paths to report
Info (332146): Worst-case minimum pulse width slack is 9.766
    Info (332119):     Slack       End Point TNS Clock 
    Info (332119): ========= =================== =====================
    Info (332119):     9.766               0.000 iCLK 
Info (332115): Report Timing: Found 1 setup paths (1 violated).  Worst case slack is -19.255
    Info (332115): -to_clock [get_clocks {iCLK}]
    Info (332115): -setup
    Info (332115): -stdout
Info (332115): Path #1: Setup slack is -19.255 (VIOLATED)
    Info (332115): ===================================================================
    Info (332115): From Node    : fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:3:DFFI|s_Q
    Info (332115): To Node      : regfile:registers|nbit_register:\G_1:2:reg_i|dffg:\GNBit_Register:15:DFFI|s_Q
    Info (332115): Launch Clock : iCLK
    Info (332115): Latch Clock  : iCLK
    Info (332115): Data Arrival Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):      0.000      0.000           launch edge time
    Info (332115):      2.802      2.802  R        clock network delay
    Info (332115):      3.015      0.213     uTco  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:3:DFFI|s_Q
    Info (332115):      3.015      0.000 RR  CELL  fetch_unit|PC|\GNBit_Register:3:DFFI|s_Q|q
    Info (332115):      3.295      0.280 RR    IC  s_IMemAddr[3]~1|datad
    Info (332115):      3.439      0.144 RR  CELL  s_IMemAddr[3]~1|combout
    Info (332115):      5.733      2.294 RR    IC  IMem|ram~43599|datab
    Info (332115):      6.128      0.395 RF  CELL  IMem|ram~43599|combout
    Info (332115):      6.337      0.209 FF    IC  IMem|ram~43600|datad
    Info (332115):      6.471      0.134 FR  CELL  IMem|ram~43600|combout
    Info (332115):      7.665      1.194 RR    IC  IMem|ram~43603|datad
    Info (332115):      7.809      0.144 RR  CELL  IMem|ram~43603|combout
    Info (332115):      7.997      0.188 RR    IC  IMem|ram~43606|datad
    Info (332115):      8.141      0.144 RR  CELL  IMem|ram~43606|combout
    Info (332115):      8.824      0.683 RR    IC  IMem|ram~43638|datac
    Info (332115):      9.089      0.265 RR  CELL  IMem|ram~43638|combout
    Info (332115):      9.278      0.189 RR    IC  IMem|ram~43681|datad
    Info (332115):      9.422      0.144 RR  CELL  IMem|ram~43681|combout
    Info (332115):     13.268      3.846 RR    IC  IMem|ram~43724|datad
    Info (332115):     13.412      0.144 RR  CELL  IMem|ram~43724|combout
    Info (332115):     13.601      0.189 RR    IC  IMem|ram~43725|datad
    Info (332115):     13.745      0.144 RR  CELL  IMem|ram~43725|combout
    Info (332115):     16.580      2.835 RR    IC  registers|mux_read_B|Mux12~9|datad
    Info (332115):     16.724      0.144 RR  CELL  registers|mux_read_B|Mux12~9|combout
    Info (332115):     16.913      0.189 RR    IC  registers|mux_read_B|Mux12~10|datad
    Info (332115):     17.057      0.144 RR  CELL  registers|mux_read_B|Mux12~10|combout
    Info (332115):     20.840      3.783 RR    IC  registers|mux_read_B|Mux12~11|dataa
    Info (332115):     21.147      0.307 RR  CELL  registers|mux_read_B|Mux12~11|combout
    Info (332115):     21.336      0.189 RR    IC  registers|mux_read_B|Mux12~16|datad
    Info (332115):     21.480      0.144 RR  CELL  registers|mux_read_B|Mux12~16|combout
    Info (332115):     21.667      0.187 RR    IC  registers|mux_read_B|Mux12~19|datad
    Info (332115):     21.811      0.144 RR  CELL  registers|mux_read_B|Mux12~19|combout
    Info (332115):     22.581      0.770 RR    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~0|datab
    Info (332115):     22.950      0.369 RR  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~0|combout
    Info (332115):     23.141      0.191 RR    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~1|datac
    Info (332115):     23.406      0.265 RR  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~1|combout
    Info (332115):     24.092      0.686 RR    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~0|datac
    Info (332115):     24.357      0.265 RR  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~0|combout
    Info (332115):     24.541      0.184 RR    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~1|datac
    Info (332115):     24.806      0.265 RR  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~1|combout
    Info (332115):     25.504      0.698 RR    IC  arithmetic_logic_unit|Mux27~0|datad
    Info (332115):     25.648      0.144 RR  CELL  arithmetic_logic_unit|Mux27~0|combout
    Info (332115):     25.837      0.189 RR    IC  arithmetic_logic_unit|Mux27~1|datad
    Info (332115):     25.981      0.144 RR  CELL  arithmetic_logic_unit|Mux27~1|combout
    Info (332115):     26.168      0.187 RR    IC  arithmetic_logic_unit|Mux27~2|datad
    Info (332115):     26.312      0.144 RR  CELL  arithmetic_logic_unit|Mux27~2|combout
    Info (332115):     26.501      0.189 RR    IC  arithmetic_logic_unit|Mux27~3|datad
    Info (332115):     26.645      0.144 RR  CELL  arithmetic_logic_unit|Mux27~3|combout
    Info (332115):     26.834      0.189 RR    IC  arithmetic_logic_unit|Mux27~4|datad
    Info (332115):     26.978      0.144 RR  CELL  arithmetic_logic_unit|Mux27~4|combout
    Info (332115):     27.165      0.187 RR    IC  arithmetic_logic_unit|Mux27~5|datad
    Info (332115):     27.309      0.144 RR  CELL  arithmetic_logic_unit|Mux27~5|combout
    Info (332115):     27.497      0.188 RR    IC  arithmetic_logic_unit|Mux27~6|datad
    Info (332115):     27.641      0.144 RR  CELL  arithmetic_logic_unit|Mux27~6|combout
    Info (332115):     27.829      0.188 RR    IC  arithmetic_logic_unit|Mux27~7|datad
    Info (332115):     27.973      0.144 RR  CELL  arithmetic_logic_unit|Mux27~7|combout
    Info (332115):     28.161      0.188 RR    IC  arithmetic_logic_unit|Mux27~8|datad
    Info (332115):     28.305      0.144 RR  CELL  arithmetic_logic_unit|Mux27~8|combout
    Info (332115):     30.615      2.310 RR    IC  DMem|ram~46153|datab
    Info (332115):     31.010      0.395 RF  CELL  DMem|ram~46153|combout
    Info (332115):     31.217      0.207 FF    IC  DMem|ram~46154|datad
    Info (332115):     31.351      0.134 FR  CELL  DMem|ram~46154|combout
    Info (332115):     31.700      0.349 RR    IC  DMem|ram~46157|datac
    Info (332115):     31.965      0.265 RR  CELL  DMem|ram~46157|combout
    Info (332115):     32.182      0.217 RR    IC  DMem|ram~46160|datab
    Info (332115):     32.563      0.381 RR  CELL  DMem|ram~46160|combout
    Info (332115):     35.906      3.343 RR    IC  DMem|ram~46192|datab
    Info (332115):     36.270      0.364 RR  CELL  DMem|ram~46192|combout
    Info (332115):     36.457      0.187 RR    IC  DMem|ram~46235|datad
    Info (332115):     36.601      0.144 RR  CELL  DMem|ram~46235|combout
    Info (332115):     36.789      0.188 RR    IC  DMem|ram~46278|datad
    Info (332115):     36.933      0.144 RR  CELL  DMem|ram~46278|combout
    Info (332115):     37.859      0.926 RR    IC  DMem|ram~46279|datac
    Info (332115):     38.124      0.265 RR  CELL  DMem|ram~46279|combout
    Info (332115):     38.341      0.217 RR    IC  DMem|ram~46450|datab
    Info (332115):     38.722      0.381 RR  CELL  DMem|ram~46450|combout
    Info (332115):     38.911      0.189 RR    IC  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~2|datad
    Info (332115):     39.055      0.144 RR  CELL  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~2|combout
    Info (332115):     39.242      0.187 RR    IC  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~3|datad
    Info (332115):     39.386      0.144 RR  CELL  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~3|combout
    Info (332115):     41.574      2.188 RR    IC  registers|\G_1:2:reg_i|\GNBit_Register:15:DFFI|s_Q|asdata
    Info (332115):     41.944      0.370 RR  CELL  regfile:registers|nbit_register:\G_1:2:reg_i|dffg:\GNBit_Register:15:DFFI|s_Q
    Info (332115): Data Required Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):     20.000     20.000           latch edge time
    Info (332115):     22.683      2.683  R        clock network delay
    Info (332115):     22.690      0.007           clock pessimism removed
    Info (332115):     22.670     -0.020           clock uncertainty
    Info (332115):     22.689      0.019     uTsu  regfile:registers|nbit_register:\G_1:2:reg_i|dffg:\GNBit_Register:15:DFFI|s_Q
    Info (332115): Data Arrival Time  :    41.944
    Info (332115): Data Required Time :    22.689
    Info (332115): Slack              :   -19.255 (VIOLATED)
    Info (332115): ===================================================================
Info (332115): Report Timing: Found 1 hold paths (0 violated).  Worst case slack is 0.355
    Info (332115): -to_clock [get_clocks {iCLK}]
    Info (332115): -hold
    Info (332115): -stdout
Info (332115): Path #1: Hold slack is 0.355 
    Info (332115): ===================================================================
    Info (332115): From Node    : fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): To Node      : fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): Launch Clock : iCLK
    Info (332115): Latch Clock  : iCLK
    Info (332115): Data Arrival Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):      0.000      0.000           launch edge time
    Info (332115):      2.695      2.695  R        clock network delay
    Info (332115):      2.908      0.213     uTco  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115):      2.908      0.000 FF  CELL  fetch_unit|PC|\GNBit_Register:2:DFFI|s_Q|q
    Info (332115):      2.908      0.000 FF    IC  fetch_unit|pc_rst_mux|\G_NBit_MUX:2:MUXI|AND1|o_F~2|datac
    Info (332115):      3.227      0.319 FF  CELL  fetch_unit|pc_rst_mux|\G_NBit_MUX:2:MUXI|AND1|o_F~2|combout
    Info (332115):      3.227      0.000 FF    IC  fetch_unit|PC|\GNBit_Register:2:DFFI|s_Q|d
    Info (332115):      3.292      0.065 FF  CELL  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): Data Required Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):      0.000      0.000           latch edge time
    Info (332115):      2.794      2.794  R        clock network delay
    Info (332115):      2.766     -0.028           clock pessimism removed
    Info (332115):      2.766      0.000           clock uncertainty
    Info (332115):      2.937      0.171      uTh  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): Data Arrival Time  :     3.292
    Info (332115): Data Required Time :     2.937
    Info (332115): Slack              :     0.355 
    Info (332115): ===================================================================
Info: Analyzing Fast 1200mV 0C Model
Critical Warning (332148): Timing requirements not met
    Info (11105): For recommendations on closing timing, run Report Timing Closure Recommendations in the Timing Analyzer.
Info (332146): Worst-case setup slack is -2.322
    Info (332119):     Slack       End Point TNS Clock 
    Info (332119): ========= =================== =====================
    Info (332119):    -2.322            -870.088 iCLK 
Info (332146): Worst-case hold slack is 0.182
    Info (332119):     Slack       End Point TNS Clock 
    Info (332119): ========= =================== =====================
    Info (332119):     0.182               0.000 iCLK 
Info (332140): No Recovery paths to report
Info (332140): No Removal paths to report
Info (332146): Worst-case minimum pulse width slack is 9.404
    Info (332119):     Slack       End Point TNS Clock 
    Info (332119): ========= =================== =====================
    Info (332119):     9.404               0.000 iCLK 
Info (332115): Report Timing: Found 1 setup paths (1 violated).  Worst case slack is -2.322
    Info (332115): -to_clock [get_clocks {iCLK}]
    Info (332115): -setup
    Info (332115): -stdout
Info (332115): Path #1: Setup slack is -2.322 (VIOLATED)
    Info (332115): ===================================================================
    Info (332115): From Node    : fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:3:DFFI|s_Q
    Info (332115): To Node      : regfile:registers|nbit_register:\G_1:2:reg_i|dffg:\GNBit_Register:15:DFFI|s_Q
    Info (332115): Launch Clock : iCLK
    Info (332115): Latch Clock  : iCLK
    Info (332115): Data Arrival Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):      0.000      0.000           launch edge time
    Info (332115):      1.646      1.646  R        clock network delay
    Info (332115):      1.751      0.105     uTco  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:3:DFFI|s_Q
    Info (332115):      1.751      0.000 FF  CELL  fetch_unit|PC|\GNBit_Register:3:DFFI|s_Q|q
    Info (332115):      1.940      0.189 FF    IC  s_IMemAddr[3]~1|datad
    Info (332115):      2.003      0.063 FF  CELL  s_IMemAddr[3]~1|combout
    Info (332115):      3.393      1.390 FF    IC  IMem|ram~43599|datab
    Info (332115):      3.604      0.211 FR  CELL  IMem|ram~43599|combout
    Info (332115):      3.696      0.092 RR    IC  IMem|ram~43600|datad
    Info (332115):      3.762      0.066 RF  CELL  IMem|ram~43600|combout
    Info (332115):      4.445      0.683 FF    IC  IMem|ram~43603|datad
    Info (332115):      4.508      0.063 FF  CELL  IMem|ram~43603|combout
    Info (332115):      4.616      0.108 FF    IC  IMem|ram~43606|datad
    Info (332115):      4.679      0.063 FF  CELL  IMem|ram~43606|combout
    Info (332115):      5.069      0.390 FF    IC  IMem|ram~43638|datac
    Info (332115):      5.202      0.133 FF  CELL  IMem|ram~43638|combout
    Info (332115):      5.311      0.109 FF    IC  IMem|ram~43681|datad
    Info (332115):      5.374      0.063 FF  CELL  IMem|ram~43681|combout
    Info (332115):      7.789      2.415 FF    IC  IMem|ram~43724|datad
    Info (332115):      7.852      0.063 FF  CELL  IMem|ram~43724|combout
    Info (332115):      7.962      0.110 FF    IC  IMem|ram~43725|datad
    Info (332115):      8.025      0.063 FF  CELL  IMem|ram~43725|combout
    Info (332115):      9.642      1.617 FF    IC  registers|mux_read_B|Mux12~9|datad
    Info (332115):      9.705      0.063 FF  CELL  registers|mux_read_B|Mux12~9|combout
    Info (332115):      9.814      0.109 FF    IC  registers|mux_read_B|Mux12~10|datad
    Info (332115):      9.877      0.063 FF  CELL  registers|mux_read_B|Mux12~10|combout
    Info (332115):     12.027      2.150 FF    IC  registers|mux_read_B|Mux12~11|dataa
    Info (332115):     12.200      0.173 FF  CELL  registers|mux_read_B|Mux12~11|combout
    Info (332115):     12.309      0.109 FF    IC  registers|mux_read_B|Mux12~16|datad
    Info (332115):     12.372      0.063 FF  CELL  registers|mux_read_B|Mux12~16|combout
    Info (332115):     12.479      0.107 FF    IC  registers|mux_read_B|Mux12~19|datad
    Info (332115):     12.542      0.063 FF  CELL  registers|mux_read_B|Mux12~19|combout
    Info (332115):     13.002      0.460 FF    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~0|datab
    Info (332115):     13.195      0.193 FF  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~0|combout
    Info (332115):     13.312      0.117 FF    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~1|datac
    Info (332115):     13.445      0.133 FF  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase2:17:MuxShf|o_O~1|combout
    Info (332115):     13.836      0.391 FF    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~0|datac
    Info (332115):     13.969      0.133 FF  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~0|combout
    Info (332115):     14.078      0.109 FF    IC  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~1|datac
    Info (332115):     14.211      0.133 FF  CELL  arithmetic_logic_unit|ALU_SRL|\ShiftPhase3:13:MuxShf|o_O~1|combout
    Info (332115):     14.607      0.396 FF    IC  arithmetic_logic_unit|Mux27~0|datad
    Info (332115):     14.670      0.063 FF  CELL  arithmetic_logic_unit|Mux27~0|combout
    Info (332115):     14.779      0.109 FF    IC  arithmetic_logic_unit|Mux27~1|datad
    Info (332115):     14.842      0.063 FF  CELL  arithmetic_logic_unit|Mux27~1|combout
    Info (332115):     14.950      0.108 FF    IC  arithmetic_logic_unit|Mux27~2|datad
    Info (332115):     15.013      0.063 FF  CELL  arithmetic_logic_unit|Mux27~2|combout
    Info (332115):     15.122      0.109 FF    IC  arithmetic_logic_unit|Mux27~3|datad
    Info (332115):     15.185      0.063 FF  CELL  arithmetic_logic_unit|Mux27~3|combout
    Info (332115):     15.294      0.109 FF    IC  arithmetic_logic_unit|Mux27~4|datad
    Info (332115):     15.357      0.063 FF  CELL  arithmetic_logic_unit|Mux27~4|combout
    Info (332115):     15.464      0.107 FF    IC  arithmetic_logic_unit|Mux27~5|datad
    Info (332115):     15.527      0.063 FF  CELL  arithmetic_logic_unit|Mux27~5|combout
    Info (332115):     15.634      0.107 FF    IC  arithmetic_logic_unit|Mux27~6|datad
    Info (332115):     15.697      0.063 FF  CELL  arithmetic_logic_unit|Mux27~6|combout
    Info (332115):     15.804      0.107 FF    IC  arithmetic_logic_unit|Mux27~7|datad
    Info (332115):     15.867      0.063 FF  CELL  arithmetic_logic_unit|Mux27~7|combout
    Info (332115):     15.974      0.107 FF    IC  arithmetic_logic_unit|Mux27~8|datad
    Info (332115):     16.037      0.063 FF  CELL  arithmetic_logic_unit|Mux27~8|combout
    Info (332115):     17.511      1.474 FF    IC  DMem|ram~46153|datab
    Info (332115):     17.722      0.211 FR  CELL  DMem|ram~46153|combout
    Info (332115):     17.812      0.090 RR    IC  DMem|ram~46154|datad
    Info (332115):     17.878      0.066 RF  CELL  DMem|ram~46154|combout
    Info (332115):     18.062      0.184 FF    IC  DMem|ram~46157|datac
    Info (332115):     18.195      0.133 FF  CELL  DMem|ram~46157|combout
    Info (332115):     18.324      0.129 FF    IC  DMem|ram~46160|datab
    Info (332115):     18.516      0.192 FF  CELL  DMem|ram~46160|combout
    Info (332115):     20.511      1.995 FF    IC  DMem|ram~46192|datab
    Info (332115):     20.718      0.207 FF  CELL  DMem|ram~46192|combout
    Info (332115):     20.825      0.107 FF    IC  DMem|ram~46235|datad
    Info (332115):     20.888      0.063 FF  CELL  DMem|ram~46235|combout
    Info (332115):     20.996      0.108 FF    IC  DMem|ram~46278|datad
    Info (332115):     21.059      0.063 FF  CELL  DMem|ram~46278|combout
    Info (332115):     21.572      0.513 FF    IC  DMem|ram~46279|datac
    Info (332115):     21.705      0.133 FF  CELL  DMem|ram~46279|combout
    Info (332115):     21.836      0.131 FF    IC  DMem|ram~46450|datab
    Info (332115):     22.029      0.193 FF  CELL  DMem|ram~46450|combout
    Info (332115):     22.138      0.109 FF    IC  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~2|datad
    Info (332115):     22.201      0.063 FF  CELL  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~2|combout
    Info (332115):     22.307      0.106 FF    IC  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~3|datad
    Info (332115):     22.370      0.063 FF  CELL  reg_write_data_mux|\G_NBit_MUX:15:MUXI|OR1|o_F~3|combout
    Info (332115):     23.709      1.339 FF    IC  registers|\G_1:2:reg_i|\GNBit_Register:15:DFFI|s_Q|asdata
    Info (332115):     23.884      0.175 FF  CELL  regfile:registers|nbit_register:\G_1:2:reg_i|dffg:\GNBit_Register:15:DFFI|s_Q
    Info (332115): Data Required Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):     20.000     20.000           latch edge time
    Info (332115):     21.570      1.570  R        clock network delay
    Info (332115):     21.575      0.005           clock pessimism removed
    Info (332115):     21.555     -0.020           clock uncertainty
    Info (332115):     21.562      0.007     uTsu  regfile:registers|nbit_register:\G_1:2:reg_i|dffg:\GNBit_Register:15:DFFI|s_Q
    Info (332115): Data Arrival Time  :    23.884
    Info (332115): Data Required Time :    21.562
    Info (332115): Slack              :    -2.322 (VIOLATED)
    Info (332115): ===================================================================
Info (332115): Report Timing: Found 1 hold paths (0 violated).  Worst case slack is 0.182
    Info (332115): -to_clock [get_clocks {iCLK}]
    Info (332115): -hold
    Info (332115): -stdout
Info (332115): Path #1: Hold slack is 0.182 
    Info (332115): ===================================================================
    Info (332115): From Node    : fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): To Node      : fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): Launch Clock : iCLK
    Info (332115): Latch Clock  : iCLK
    Info (332115): Data Arrival Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):      0.000      0.000           launch edge time
    Info (332115):      1.577      1.577  R        clock network delay
    Info (332115):      1.682      0.105     uTco  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115):      1.682      0.000 RR  CELL  fetch_unit|PC|\GNBit_Register:2:DFFI|s_Q|q
    Info (332115):      1.682      0.000 RR    IC  fetch_unit|pc_rst_mux|\G_NBit_MUX:2:MUXI|AND1|o_F~2|datac
    Info (332115):      1.853      0.171 RR  CELL  fetch_unit|pc_rst_mux|\G_NBit_MUX:2:MUXI|AND1|o_F~2|combout
    Info (332115):      1.853      0.000 RR    IC  fetch_unit|PC|\GNBit_Register:2:DFFI|s_Q|d
    Info (332115):      1.884      0.031 RR  CELL  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): Data Required Path:
    Info (332115): Total (ns)  Incr (ns)     Type  Element
    Info (332115): ==========  ========= ==  ====  ===================================
    Info (332115):      0.000      0.000           latch edge time
    Info (332115):      1.638      1.638  R        clock network delay
    Info (332115):      1.618     -0.020           clock pessimism removed
    Info (332115):      1.618      0.000           clock uncertainty
    Info (332115):      1.702      0.084      uTh  fetch:fetch_unit|nbit_register:PC|dffg:\GNBit_Register:2:DFFI|s_Q
    Info (332115): Data Arrival Time  :     1.884
    Info (332115): Data Required Time :     1.702
    Info (332115): Slack              :     0.182 
    Info (332115): ===================================================================
Info (332102): Design is not fully constrained for setup requirements
Info (332102): Design is not fully constrained for hold requirements
Info: Quartus Prime Timing Analyzer was successful. 0 errors, 4 warnings
    Info: Peak virtual memory: 2815 megabytes
    Info: Processing ended: Tue Nov 15 19:48:56 2022
    Info: Elapsed time: 00:01:37
    Info: Total CPU time (on all processors): 00:01:52
